package employee;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import soos.employee.Employee;
import soos.employee.EmployeeFactory;
import soos.employee.Medic;
import soos.employee.Technician;
import soos.exceptions.SoosEX;
import soos.exceptions.invalid.InvalidFieldEX;

public class EmployeeFactoryTest {
	
	EmployeeFactory factory;
	
	@Before @Test
	public void testCreateEmployee() {
		factory = new EmployeeFactory();
		factoryTest();
		exceptionTest();
	}
	
	@Test
	public void factoryTest() {
		try {
			Employee joao = factory.createEmployee("Joao", "21/08/1986", "Tecnico Administrativo", "1");
			Employee maria = factory.createEmployee("Maria", "20/12/1980", "Medico", "2");
			
			assertEquals("Joao", joao.getName());
			assertEquals("Maria", maria.getName());
			
			assertTrue(joao instanceof Technician);
			assertTrue(maria instanceof Medic);
			
		} catch (SoosEX e) {
			System.out.println(e.getMessage());
			fail();
		}
	}
	
	@Test
	public void exceptionTest() {
		try {
			Employee joao = factory.createEmployee("", "21/08/1986", "Tecnico Administrativo", "1");
			fail();
		} catch (SoosEX e) {
			assertEquals("Erro no cadastro de funcionario. Nome do funcionario nao pode ser vazio.",
					e.getMessage());
		}
		
		try {
			Employee joao = factory.createEmployee("Joao", "21/08/1986", "Tecnico", "1");
			fail();
		} catch (SoosEX e) {
			assertEquals("Erro no cadastro de funcionario. Cargo invalido.", e.getMessage());
		}
	}

}
