package employee;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import soos.employee.Director;
import soos.employee.Employee;
import soos.employee.Medic;
import soos.employee.Technician;
import soos.exceptions.SoosEX;

public class EmployeeTest {
	
	Employee director;
	Employee medic;
	Employee technician;
	
	@Before @Test
	public void setup() {
		employeeTest();
		exceptionsTest();
	}
	
	@Test
	public void employeeTest() {
		
		try {
			director = new Director("Marie Curie", "07/11/1967", "1");
			medic = new Medic("Ada Lovelace", "10/12/1975", "2");
			technician = new Technician("Erasmo de Rotterdam", "28/10/1966", "3");
			
			assertEquals("Marie Curie", director.getName());
			assertEquals("Ada Lovelace", medic.getName());
			assertEquals("Erasmo de Rotterdam", technician.getName());			
			
		} catch (SoosEX e) {
			fail();
		}
	}
	
	@Test
	public void exceptionsTest() {
		
		try {
			director = new Director("", "07/11/1967", "1");
			fail();
		} catch (SoosEX e) {
			assertEquals("Erro no cadastro de funcionario. Nome do funcionario nao pode ser vazio.",
					e.getMessage());
		}
		
		try {
			medic = new Medic("Ada Lovelace", "10-12-1975", "2");
			fail();
		} catch (SoosEX e) {
			assertEquals("Erro no cadastro de funcionario. Data invalida.",
					e.getMessage());
		}
		
		try {
			technician.changePassword("senhaerrada", "novasenha");
			fail();
		} catch (SoosEX e) {
			assertEquals("Erro ao atualizar funcionario. Senha invalida.", e.getMessage());
		}
	}

}
