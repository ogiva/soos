package controller;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import soos.controller.Pharmacy;

public class PharmacyTest {
	
	Pharmacy pharmacy = new Pharmacy();

	@Test
	public void testAddMedicine() {
		try {
			assertEquals("Dipirona", pharmacy.addMedicine("Dipirona", "Referencia", 20, 20, "analgesico"));
			assertEquals(pharmacy.containsMedicines("Dipirona"), 20, 0.1);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void testGetMedicineInfo() {
		try {
			pharmacy.addMedicine("Dipirona", "Referencia", 20, 20, "analgesico");
			assertEquals(pharmacy.getMedicineInfo("Preco", "Dipirona"), "20.0");
			assertEquals(pharmacy.getMedicineInfo("Tipo", "Dipirona"), "de Referencia");
			assertEquals(pharmacy.getMedicineInfo("Quantidade", "Dipirona"), "20");
			assertEquals(pharmacy.getMedicineInfo("Preco", "Ibuprofeno"), "20.0");
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Erro ao consultar medicamento. Medicamento nao cadastrado.");
		}
		
		try {
			pharmacy.addMedicine("Dipirona", "Referencia", 20, 20, "analgesico");
			assertEquals(pharmacy.getMedicineInfo("Valor", "Dipirona"), "20");
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Erro ao consultar medicamento. Atributo invalido.");
		}
	}

	@Test
	public void testUpdateMedicineInfo() {
		try {
			pharmacy.addMedicine("Dipirona", "Referencia", 20, 20, "analgesico");
			pharmacy.updateMedicineInfo("Dipirona", "Quantidade", "10");
			assertEquals(pharmacy.getMedicineInfo("Quantidade", "Dipirona"), "10");
			pharmacy.updateMedicineInfo("Dipirona", "Preco", "35.0");
			assertEquals(pharmacy.getMedicineInfo("Preco", "Dipirona"), "35.0");
			pharmacy.updateMedicineInfo("Dipirona", "Nome", "NaoPode");
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Erro ao atualizar medicamento. Nome do medicamento nao pode ser alterado.");
		}
		
		try {
			pharmacy.addMedicine("Dipirona", "Referencia", 20, 20, "analgesico");
			pharmacy.updateMedicineInfo("Dipirona", "Tipo", "NaoPode");
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Erro ao atualizar medicamento. Tipo do medicamento nao pode ser alterado.");
		}
		
	}

	@Test
	public void testMedicineSearchByName() {
		try {
			pharmacy.addMedicine("Dipirona", "Referencia", 20, 20, "analgesico");
			pharmacy.addMedicine("Ibuprofeno", "Generico", 20, 20, "analgesico");
			pharmacy.addMedicine("Tylenol", "Referencia", 20, 20, "analgesico");
			assertEquals(pharmacy.medicineSearchByName("Dipirona"), 
					"Medicamento de Referencia: Dipirona - Preco: R$ 20,00 - Disponivel: 20 - Categorias: analgesico");
			assertEquals(pharmacy.medicineSearchByName("Ibuprofeno"), 
					"Medicamento Generico: Ibuprofeno - Preco: R$ 12,00 - Disponivel: 20 - Categorias: analgesico");
			assertEquals(pharmacy.medicineSearchByName("Tylenol"), 
					"Medicamento de Referencia: Tylenol - Preco: R$ 20,00 - Disponivel: 20 - Categorias: analgesico");
			pharmacy.medicineSearchByName("NaoExiste");
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Erro na consulta de medicamentos. Medicamento nao cadastrado.");
		}
		
	}

	@Test
	public void testMedicineSearchByCategory() {
		try {
			pharmacy.addMedicine("Dipirona", "Referencia", 20, 20, "analgesico");
			pharmacy.addMedicine("Ibuprofeno", "Generico", 20, 20, "antibiotico");
			pharmacy.addMedicine("Tylenol", "Referencia", 20, 20, "analgesico");
			assertEquals(pharmacy.medicineSearchByCategory("analgesico"), "Dipirona,Tylenol");
			assertEquals(pharmacy.medicineSearchByCategory("antibiotico"), "Ibuprofeno");
			pharmacy.medicineSearchByCategory("hormonal");
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Erro na consulta de medicamentos. Nao ha remedios cadastrados nessa categoria.");
		}
	}

	@Test
	public void testMedicineSearchAll() {
		try {
			pharmacy.addMedicine("Dipirona", "Referencia", 10, 20, "analgesico");
			pharmacy.addMedicine("Ibuprofeno", "Generico", 20, 20, "antibiotico");
			pharmacy.addMedicine("Tylenol", "Referencia", 15, 20, "analgesico");
			assertEquals(pharmacy.medicineSearchAll("Preco"), "Dipirona,Ibuprofeno,Tylenol");
			assertEquals(pharmacy.medicineSearchAll("alfabetica"), "Dipirona,Ibuprofeno,Tylenol");
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void testContainsMedicines() {
		try {
			pharmacy.addMedicine("Dipirona", "Referencia", 10, 20, "analgesico");
			pharmacy.addMedicine("Ibuprofeno", "Generico", 20, 20, "antibiotico");
			pharmacy.addMedicine("Tylenol", "Referencia", 15, 20, "analgesico");
			assertEquals(pharmacy.containsMedicines("Dipirona,Tylenol,Ibuprofeno"), 37, 0.1);
			pharmacy.containsMedicines("NaoExiste");
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Erro na realizacao de procedimentos. Medicamento nao cadastrado.");
		}
	}

}
