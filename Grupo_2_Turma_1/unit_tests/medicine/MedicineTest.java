package medicine;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import soos.medicine.Generic;
import soos.medicine.Medicine;
import soos.medicine.MedicineCategoryEnum;
import soos.medicine.RefMedicine;

import java.util.ArrayList;
public class MedicineTest {
	
	ArrayList<MedicineCategoryEnum> cat = new ArrayList<MedicineCategoryEnum>();
	
	@Before
	public void inicialization() {
		cat.add(MedicineCategoryEnum.ANTI_INFLAMMATORY);
		cat.add(MedicineCategoryEnum.ANTIPYRETIC);
	}
 
	@Test
	public void medicineCriationTest() {
		try {
			Medicine teste1 = new RefMedicine("Teste", 25, 30, cat);
			Medicine teste2 = new RefMedicine("Teste", 0, 0, cat);
			Medicine teste3 = new Generic("Teste", 25, 30, cat);
			assertEquals(teste1.getName(), "Teste");
			assertEquals(teste3.getPrice(), 15, 0.01);
			assertEquals(teste1.getPrice(), 25, 0.01);
		} catch (Exception e) {
			fail();
		}
		
		try {
			Medicine erroNome = new RefMedicine(" ", 25, 30, cat);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Erro no cadastro de medicamento. Nome do medicamento nao pode ser vazio.");
		}
		
		try {
			Medicine devePassar = new RefMedicine("Medicamento", 0, 12, cat);
			Medicine erroPreco = new RefMedicine("Medicamento", -1, 12, cat);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Erro no cadastro de medicamento. Preco do medicamento nao pode ser negativo.");
		}
		
		try {
			Medicine devePassar = new Generic("Medicamento", 0, 12, cat);
			Medicine erroPreco = new Generic("Medicamento", -1, 12, cat);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Erro no cadastro de medicamento. Preco do medicamento nao pode ser negativo.");
		}
		
		try {
			Medicine devePassar = new RefMedicine("Medicamento", 10, 0, cat);
			Medicine erroEstoque = new RefMedicine("Medicamento", 10, -1, cat);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Erro no cadastro de medicamento. Quantidade do medicamento nao pode ser negativo.");
		}
		
		try {
			Medicine precoReferencia = new RefMedicine("Medicamento", 10, 1, cat);
			Medicine precoGenerico = new Generic("Medicamento", 10, 1, cat);
			assertEquals(10, precoReferencia.getPrice(), 0.1);
			assertEquals(6, precoGenerico.getPrice(), 0.1);
		} catch (Exception e) {
			System.err.print(e.getMessage());
		}
		
		try {
			Medicine teste1 = new RefMedicine("Medicamento", 10, 1, null);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Erro no cadastro do medicamento. Categoria não pode ser vazia.");
		}
		
	}

}
