package procedures;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import soos.exceptions.SoosEX;
import soos.patient.MedicalRecord;
import soos.procedures.BariatricSurgery;
import soos.procedures.MedicalAppointment;
import soos.procedures.OrganTransplant;
import soos.procedures.ProcedureFactory;
import soos.procedures.ProcedureIF;
import soos.procedures.SexualReassignment;

public class ProcedureFactoryTest {

	ProcedureFactory factory;;
	MedicalRecord patient;
	
	@Before @Test
	public void testCreateProcedure() {
		factory = new ProcedureFactory();
		try {
			patient = new MedicalRecord("Joao", "21/08/1986", 65.0, "masculino", "masculino", "O+", "1");
		} catch (SoosEX e) {
			e.printStackTrace();
		}
			
	}
	
	@Test
	public void factoryTest() {
		try {
			ProcedureIF bariatrica = factory.createProcedure("Maria", "Cirurgia Bariatrica", patient, "");
			assertTrue(bariatrica instanceof BariatricSurgery);
			
			ProcedureIF redesignacao = factory.createProcedure("Maria", "Redesignacao Sexual", patient, "");
			assertTrue(redesignacao instanceof SexualReassignment);
			
			ProcedureIF appointment = factory.createProcedure("Maria", "Consulta Clinica", patient, "");
			assertTrue(appointment instanceof MedicalAppointment);
			
			ProcedureIF transplant = factory.createProcedure("Maria", "Transplante de Orgaos", patient, "coracao");
			assertTrue(transplant instanceof OrganTransplant);
			
		} catch (SoosEX e) {
			fail();
		}
		
		try {
			ProcedureIF procedimento = factory.createProcedure("Maria", "Rinoplastia", patient, "");
			fail();
		} catch (SoosEX e) {
			assertEquals("Erro na realizacao de procedimentos. Procedimento invalido.",
					e.getMessage());
		}
	}
	
}
