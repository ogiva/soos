package procedures;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import soos.exceptions.SoosEX;
import soos.organbank.Organ;
import soos.patient.BloodTypeEnum;
import soos.patient.MedicalRecord;
import soos.procedures.BariatricSurgery;
import soos.procedures.MedicalAppointment;
import soos.procedures.OrganTransplant;
import soos.procedures.ProcedureIF;
import soos.procedures.SexualReassignment;

public class ProcedureTest {
	
	ProcedureIF procedure;
	MedicalRecord patient;	
	
	@Before @Test
	public void test() {
		try {
			patient = new MedicalRecord("Joao", "21/08/1986", 65.0, "masculino", "masculino", "O+", "1");
		} catch (SoosEX e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testProcedure() {
		try {
			procedure = new BariatricSurgery("Maria", patient);
			assertEquals(65.0, patient.getWeight(), 0.01);
			procedure.performProcedure();
			patient.registerProcedure(procedure);
			assertEquals(58.5, patient.getWeight(), 0.01);
			assertEquals(1, patient.getNumberOfProcedures());
			
			procedure = new SexualReassignment("Maria", patient);
			assertEquals("masculino", patient.getGender());
			procedure.performProcedure();
			patient.registerProcedure(procedure);
			assertNotEquals("masculino", patient.getGender());
			assertEquals("feminino", patient.getGender());
			assertEquals(2, patient.getNumberOfProcedures());
			
			
			procedure = new MedicalAppointment("Maria", patient);
			procedure.performProcedure();
			patient.registerProcedure(procedure);
			assertEquals(3, patient.getNumberOfProcedures());
			
			procedure = new OrganTransplant("Maria", patient, "coracao");
			procedure.performProcedure();
			patient.registerProcedure(procedure);
			assertEquals(4, patient.getNumberOfProcedures());
			
		} catch (SoosEX e) {
			fail();
		}
		
	}
	
}
