package patient;

import static org.junit.Assert.*;

import org.junit.Test;

import soos.patient.Patient;

public class PatientTest {

	@Test
	public void testPatient() {
		try {
			Patient plutarco = new Patient("Plutarco", "19/01/1979", 85.00, "masculino", "masculino", "O-", "1");
		} catch(Exception e) {
			fail(); // should not throw exceptions
		}
		
		try {
			Patient plutarco = new Patient(" ", "19/01/1979", 85.00, "masculino", "masculino", "O-", "2");
		} catch(Exception e) {
			assertEquals(e.getMessage(), "Nao foi possivel cadastrar o paciente. Nome do paciente nao pode ser vazio.");
		}
		
		try {
			Patient plutarco = new Patient("Plutarco", "19/13/1979", 85.00, "masculino", "masculino", "O-", "3");
		} catch(Exception e) {
			assertEquals(e.getMessage(), "Erro no cadastro de funcionario. Data invalida.");
		}
		
		try {
			Patient plutarco = new Patient("Plutarco", "19/12/1979", -85.00, "masculino", "masculino", "O-", "4");
		} catch(Exception e) {
			assertEquals(e.getMessage(), "Nao foi possivel cadastrar o paciente. Peso do paciente nao pode ser negativo.");
		}
		
		try {
			Patient plutarco = new Patient("Plutarco", "19/12/1979", 85.00, " ", "masculino", "O-", "5");
		} catch(Exception e) {
			assertEquals(e.getMessage(), "Nao foi possivel cadastrar o paciente. Sexo do paciente nao pode ser vazio.");
		}
		
		try {
			Patient plutarco = new Patient("Plutarco", "19/12/1979", 85.00, null, "masculino", "O-", "6");
		} catch(Exception e) {
			assertEquals(e.getMessage(), "Nao foi possivel cadastrar o paciente. Sexo do paciente nao pode ser null.");
		}
		
		try {
			Patient plutarco = new Patient("Plutarco", "19/12/1979", 85.00, "masculino", " ", "O-", "7");
		} catch(Exception e) {
			assertEquals(e.getMessage(), "Nao foi possivel cadastrar o paciente. Genero do paciente nao pode ser vazio.");
		}
		
		try {
			Patient plutarco = new Patient("Plutarco", "19/12/1979", 85.00, "masculino", null, "O-", "8");
		} catch(Exception e) {
			assertEquals(e.getMessage(), "Nao foi possivel cadastrar o paciente. Genero do paciente nao pode ser nulo.");
		}
		
		try {
			Patient plutarco = new Patient("Plutarco", "19/12/1979", 85.00, "masculino", "masculino", "H-", "9");
		} catch(Exception e) {
			assertEquals(e.getMessage(), "Nao foi possivel cadastrar o paciente. Tipo sanguineo invalido.");
		}
	}

}
