package organbank;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import soos.exceptions.SoosEX;
import soos.organbank.Organ;
import soos.organbank.OrganFactory;

public class OrganFactoryTest {

	OrganFactory factory;
	
	@Before @Test
	public void testCreateOrgan() {
		factory = new OrganFactory();
		factoryTest();
	}
	
	@Test
	public void factoryTest() {
		
		try {
			Organ pulmao = factory.createOrgan("Pulmao", "A+");
			Organ coracao = factory.createOrgan("Coracao", "B-");
			
			assertEquals("A+", pulmao.getBT());
			assertEquals("B-", coracao.getBT());
			
		} catch (SoosEX e) {
			fail();
		}
		
		try {
			Organ pulmao = factory.createOrgan("Pulmao", "Z+");
			fail();
		} catch (SoosEX e) {
			assertEquals("O banco de orgaos apresentou um erro. Tipo sanguineo invalido.",
					e.getMessage());
		}
		
	}

}
