package organbank;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import soos.exceptions.SoosEX;
import soos.organbank.Organ;
import soos.patient.BloodTypeEnum;

public class OrganTest {

	Organ pulmao;
	Organ rim;
	Organ figado;
	Organ pancreas;

	@Before
	public void testOrgan() {
		organTest();
		exceptionsTest();
	}

	@Test
	public void organTest() {

		try {
			pulmao = new Organ("Pulmao", BloodTypeEnum.A_POSITIVE);
			rim = new Organ("Rim", BloodTypeEnum.AB_NEGATIVE);
			figado = new Organ("Figado", BloodTypeEnum.B_NEGATIVE);
			pancreas = new Organ("Pancreas", BloodTypeEnum.O_NEGATIVE);

			assertEquals("A+", pulmao.getBT());
			assertEquals("Rim", rim.getName());
			assertEquals("B-", figado.getBT());
			assertEquals("Pancreas", pancreas.getName());

		} catch (SoosEX e) {
			fail();
		}

	}

	@Test
	public void exceptionsTest() {
		
		try {
			pulmao = new Organ("    ", BloodTypeEnum.A_POSITIVE);
			fail();
		} catch (SoosEX e) {
			assertEquals("O banco de orgaos apresentou um erro. Nome do orgao nao pode ser vazio."
					, e.getMessage());
		}
		
	}

}
