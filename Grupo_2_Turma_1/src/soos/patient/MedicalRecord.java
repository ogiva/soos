package soos.patient;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import soos.exceptions.SoosEX;
import soos.exceptions.invalid.InvalidFieldEX;
import soos.procedures.ProcedureIF;

/**
 * The MedicalRecord class represents a medical record on our system.
 * It has all the information about a patient, it's responsable for altering 
 * everything that is attached do patients.
 * @author Ogiva Team
 */
public class MedicalRecord implements Comparable<MedicalRecord>, Serializable {

	private final String LINE_SEPARATOR = System.lineSeparator();
	
	private Patient patient;
	private double expenses;
	private List<ProcedureIF> procedures;
	
	/**
	 * Necessary parameters for creating a MedicalRecord.
	 * @param name - String with the name of the patient.
	 * @param birthDate - String with the birth date of the patient, on the format DD/MM/YYYY.
	 * @param weight - Double with the patient's weight.
	 * @param bioSex - String with the biological sex of the patient.
	 * @param gender - String with the gender of the patient.
	 * @param bloodType - String that represents the patient's blood type.
	 * @param id - A String with an unique identification for the patient.
	 * @throws SoosEX
	 */
	public MedicalRecord(String name, String birthDate, double weight, String bioSex,
			String gender, String bloodType, String id) throws SoosEX {
		patient = new Patient(name, birthDate, weight, bioSex, gender, bloodType, id);
		this.expenses = 0;
		this.procedures = new ArrayList<>();
	}
	
	/**
	 * Method that informs the name of the patient.
	 * @return String with the name of the patient.
	 */
	public String getName() {
		return patient.getName();
	}
	
	/**
	 * Method that informs the birth date of the patient.
	 * @return LocalDate patients birth date.
	 */
	public LocalDate getBirthDate() {
		return patient.getBirthDate();
	}
	
	/**
	 * Method that informs the patient's weight.
	 * @return Double with the patient's weight.
	 */
	public Double getWeight() {
		return patient.getWeight();
	}
	
	/**
	 * Method that informs the patient's biological sex.
	 * @return String with the patient's biological sex.
	 */
	public String getBioSex() {
		return patient.getBioSex();
	}
	
	/**
	 * Method that informs the patient's gender.
	 * @return String with the patient's gender.
	 */
	public String getGender() {
		return patient.getGender();
	}
	
	/**
	 * Method that informs the patient's blood type.
	 * @return String with the patient's blood type.
	 */
	public String getbloodType() {
		return patient.getBloodType();
	}
	
	/**
	 * Method that informs the patient's unique identification number.
	 * @return String with the patient's ID.
	 */
	public String getId() {
		return patient.getId();
	}
	
	/**
	 * Method that changes the unique identification number of the patient.
	 * @param id: String.
	 */
	public void setId(String id){
		patient.setId(id);
	}
	
	/**
	 * Method responsible for comparing different Medical Records.
	 */
	public int compareTo(MedicalRecord obj) {
		return patient.getName().compareTo(obj.getName());
	}
	
	/**
	 * Method that informs the patient's age.
	 * @return int with the patient's age.
	 */
	public Integer getAge(){
		return patient.getAge();
	}
	
	/**
	 * Method that register a procedure on the patient. 
	 */
	public void registerProcedure(ProcedureIF procedure) {
		this.procedures.add(procedure);
	}
	
	/**
	 * Method that informs the patient's number of done procedures.
	 * @return Int with the patient's number of done procedures.
	 */
	public int getNumberOfProcedures() {
		return this.procedures.size();
	}
	
	/**
	 * Method that informs the patient's expenses.
	 * @return Double with the patient's expenses.
	 */
	public double getExpenses() {
		return this.expenses;
	}
	
	/**
	 * Method that add a expense on the patient's bill.
	 * @param value: Double, patient's expense value.
	 * @throws InvalidFieldEX
	 */
	public void addExpense(double value) throws InvalidFieldEX {
		double discount = this.patient.calcExpenseDiscount(value);
		this.expenses = expenses + (value - discount);
//		this.expenses += value;
	}
	
	/**
	 * Method that adds an amount of points on the patient's fidelity card.
	 * @param points: Int
	 * @throws InvalidFieldEX
	 */
	public void addPoints(int points) throws InvalidFieldEX {
		this.patient.addPoints(points);
	}
	
	/**
	 * Method that changes the patient's weight.
	 * @param weight: Double
	 * @throws SoosEX
	 */
	public void setWeight(double weight) throws SoosEX {
		this.patient.setWeight(weight);
	}
	
	/**
	 * Method that changes the patient's biological sex.
	 * @param bioSex: String
	 */
	public void changeSex(String bioSex) {
		this.patient.setGenderProc(bioSex);
	}
	
	/**
	 * Method that informs the patient's points on it's fidelity card.
	 * @return Integer that represents the patient's amount of points.
	 */
	public int getPoints(){
		return this.patient.getPoints();
	}
	
	public String toString() {
		NumberFormat nf = new DecimalFormat("#0.00", new DecimalFormatSymbols(new Locale("pt", "BR")));
		String expenses = nf.format(this.getExpenses());
		String patientInfo = this.patient.toString();
		String fidelity = "Gasto total: R$ " + expenses + " Pontos acumulados: " + this.getPoints();
		String procedureSummary = "Resumo de Procedimentos: " + this.getNumberOfProcedures() + " procedimento(s)";
		String procedures = "";
		for(ProcedureIF procedure : this.procedures){
			procedures += procedure.toString();
		}
		return patientInfo + LINE_SEPARATOR + fidelity + LINE_SEPARATOR + procedureSummary + LINE_SEPARATOR + procedures;
	}
	
}