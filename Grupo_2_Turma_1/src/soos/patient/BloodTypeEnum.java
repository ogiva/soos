package soos.patient;

import java.io.Serializable;

import soos.exceptions.invalid.InvalidFieldEX;

/**
 * This enum enumerates all the possible blood types. Containing the representation of each BT
 * @author Ogiva Team
 *
 */
public enum BloodTypeEnum implements Serializable{
	
	A_POSITIVE("A+"), A_NEGATIVE("A-"), B_POSITIVE("B+"), B_NEGATIVE("B-"),
	AB_POSITIVE("AB+"), AB_NEGATIVE("AB-"), O_POSITIVE("O+"), O_NEGATIVE("O-");
	
	String bloodType;
	
	BloodTypeEnum(String bloodType) {
		this.bloodType = bloodType;
	}
	
	public String getBloodType() {
		return this.bloodType;
	}
	
	/**
	 * Method to check if the String received as parameter is one of the blood types.
	 * @param bt String representation of a blood type.
	 * @return A boolean, true if is one of the blood types, false else if.
	 * @throws InvalidFieldEX If it is not one of the blood types.
	 */
	public static boolean isBloodType(String bt) throws InvalidFieldEX{
		if(bt.equalsIgnoreCase(BloodTypeEnum.A_NEGATIVE.getBloodType())){
			return true;
		}else if(bt.equalsIgnoreCase(BloodTypeEnum.A_POSITIVE.getBloodType())){
			return true;
		}else if(bt.equalsIgnoreCase(BloodTypeEnum.AB_NEGATIVE.getBloodType())){
			return true;
		}else if(bt.equalsIgnoreCase(BloodTypeEnum.AB_POSITIVE.getBloodType())){
			return true;
		}else if(bt.equalsIgnoreCase(BloodTypeEnum.B_NEGATIVE.getBloodType())){
			return true;
		}else if(bt.equalsIgnoreCase(BloodTypeEnum.B_POSITIVE.getBloodType())){
			return true;
		}else if(bt.equalsIgnoreCase(BloodTypeEnum.O_NEGATIVE.getBloodType())){
			return true;
		}else if(bt.equalsIgnoreCase(BloodTypeEnum.O_POSITIVE.getBloodType())){
			return true;
		}else{
			throw new InvalidFieldEX("O banco de orgaos apresentou um erro. Tipo sanguineo invalido.");
		}
	}

}
