package soos.patient.fidelity;

import java.io.Serializable;

/**
 * The Standard class implements the interface IFidelity, to be a type of fidelity, according to its discount 
 * and bonus values.
 * @author Ogiva Team
 *
 */
public class Standard implements IFidelity, Serializable{

	private final double DISCOUNT = 0;
	private final double BONUS = 0;
	private final String TYPE = "Standard";
	
	@Override
	public double calcDiscount(double expenses) {
		return expenses * DISCOUNT; // verificar se bastava retornar 0, ou deixa assim
	}
	
	@Override
	public int calcBonusCredit(int points) {
		return (int)(points * this.BONUS);
	}
	
	@Override
	public String toString() {
		return this.TYPE;
	}
	
}