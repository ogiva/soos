package soos.patient.fidelity;

import java.io.Serializable;

/**
 * The Master class implements the interface IFidelity, to be a type of fidelity, according to its discount 
 * and bonus values.
 * @author Ogiva Team
 *
 */
public class Master implements IFidelity, Serializable{

	private final double DISCOUNT = 15;
	private final double BONUS = 0.05;
	private final String TYPE = "Master";
	
	@Override
	public double calcDiscount(double expenses) {
		return ((expenses * DISCOUNT) / 100);
	}
	
	@Override
	public int calcBonusCredit(int points) {
		return (int)(points * this.BONUS);
	}
	
	@Override
	public String toString() {
		return this.TYPE;
	}
	
}
