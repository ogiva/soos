package soos.patient.fidelity;

import java.io.Serializable;

import soos.exceptions.invalid.InvalidFieldEX;

/**
 * the FidelityCard class represents a Fidelity Card, that contains points and a type of fidelity.
 * @author Ogiva Team
 *
 */
public class FidelityCard implements Serializable{

	private int points;
	private IFidelity fidelity;
	
	/**
	 * Constructs a FidelityCard that starts with 0 points and Standard fidelity.
	 */
	public FidelityCard() {
		this.setPoints(0);
		this.turnStandard();
	}
	
	/** 
	 * Returns the fidelity card's points.
	 * @return an int that represents the points of the fidelity card.
	 */
	public int getPoints() {
		return points;
	}

	/**
	 * Changes the fidelity card's points to the given as parameter.
	 * @param points the number that will replace the fidelity card's points.
	 */
	private void setPoints(int points) {
		this.points = points;
	}

	/** 
	 * Returns the fidelity card's fidelity.
	 * @return an String that represents the points of the fidelity card.
	 */
	private String getFidelity() {
		return fidelity.toString();
	}
	
	/**
	 * Turns the card's fidelity to Standard.
	 */
	private void turnStandard(){
		this.fidelity = new Standard();
	}
	
	/**
	 * Turns the card's fidelity to Master.
	 */
	private void turnMaster(){
		this.fidelity = new Master();
	}
	
	/**
	 * Turns the card's fidelity to Vip.
	 */
	private void turnVip(){
		this.fidelity = new Vip();
	}
	
	/**
	 * Changes the card's fidelity according to the its points.
	 */
	private void changeFidelity(){
		if(this.getPoints() < 150){
			this.turnStandard();
		} else if(this.getPoints() <= 350){
			this.turnMaster();
		} else{ // if(this.getPoints() > 350)
			this.turnVip();
		}
	}
	
	/**
	 * Calculates the expenses discount according to the card's fidelity.
	 * @param expenses value to be calculated the discount
	 * @return the discount
	 * @throws InvalidFieldEX If a negative number is passed
	 */
	public double calcExpenseDiscount(double expenses) throws InvalidFieldEX{
		if(expenses < 0){
			throw new InvalidFieldEX("Erro ao adicionar gasto. Gasto nao pode ser negativo.");
		}
		return this.fidelity.calcDiscount(expenses); 
	}
	
	/**
	 * Adds points to the fidelity card, and change fidelity automaticaly, if possible.
	 * @param points the points to be added
	 * @throws InvalidFieldEX If the points parameter is negative
	 */
	public void addPoints(int points) throws InvalidFieldEX{
		if(points < 0){
			throw new InvalidFieldEX("Erro ao adicionar pontos. Pontos nao podem ser negativos.");
		}
		int finalPoints = points; //Nao calcula bonus dos pontos de acordo com Fidelity (testes futuros)
		this.setPoints(getPoints() + finalPoints);
		changeFidelity();
	}
	
	/**
	 * Returns a string that contains the card's fidelity.
	 */
	public String toString() {
		return this.fidelity.toString();
	}
	
}
