package soos.patient.fidelity;

import java.io.Serializable;

/**
 * The Vip class implements the interface IFidelity, to be a type of fidelity, according to its discount 
 * and bonus values.
 * @author Ogiva Team
 *
 */
public class Vip implements IFidelity, Serializable{

	private final double DISCOUNT = 30;
	private final double BONUS = 0.1;
	private final String TYPE = "Vip";
	
	@Override
	public double calcDiscount(double expenses) {
		return ((expenses * DISCOUNT) / 100);
	}
	
	@Override
	public int calcBonusCredit(int points) {
		return (int)(points * this.BONUS);
	}
	
	@Override
	public String toString() {
		return this.TYPE;
	}

}
