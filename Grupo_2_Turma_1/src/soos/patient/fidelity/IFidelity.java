package soos.patient.fidelity;

/**
 * The IFidelity interface represents what a Fidelity can do.
 * @author Ogiva Team
 *
 */
public interface IFidelity {

	/**
	 * This method calculates the expense discount according to the fidelity type.
	 * @param expenses the value to calculate discount
	 * @return discount value
	 */
	public abstract double calcDiscount(double expenses);
	
	/**
	 * This method calculates the bonus credit according to the fidelity type.
	 * @param points the points to calculate bonus
	 * @return bonus credit value
	 */
	public abstract int calcBonusCredit(int points);
	
	/**
	 * This method returns a String presentation that informs which type is the Fidelity.
	 * @return a String representation of fidelity type
	 */
	public abstract String toString();
	
}
