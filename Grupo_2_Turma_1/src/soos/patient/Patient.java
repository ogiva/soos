package soos.patient;

import java.io.Serializable;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import soos.exceptions.SoosEX;
import soos.exceptions.invalid.InvalidFieldEX;
import soos.patient.fidelity.FidelityCard;

/**
 * The "Patient" class describes a patient and its behavior.
 * 
 * @author Ogiva Team
 *
 */
public class Patient implements Serializable{
	private final String LINE_SEPARATOR = System.lineSeparator();

	private String name;
	private LocalDate birthDate;
	private Double weight;
	private String bioSex;
	private String gender;
	private String bloodType;
	private String id;
	private Integer age;
	private FidelityCard fidelity;
	
	/**
	 * This method creates an object of the type Patient with the given parameters.
	 * @param name the patient's name
	 * @param birthDate the patient's birth date
	 * @param weight the patient's weight
	 * @param bloodType the patient's blood type
	 * @param bioSex the patient's biological sex
	 * @param gender the patient's gender
	 * @throws Exception Ensures that the patient will not be created with invalid information.
	 */
	public Patient(String name, String birthDate, double weight, String bioSex,
			String gender, String bloodType, String id) throws SoosEX {
		this.setName(name);
		this.setBirthDate(this.getDate(birthDate));
		this.setWeight(weight);
		this.setBioSex(bioSex);
		this.setGender(gender);
		this.setBloodType(bloodType);
		this.setId(id);
		this.setAge();
		this.fidelity = new FidelityCard();
	}

	/**
	 * This method returns the name of the patient.
	 * @return a String containing the name of the patient
	 */
	public String getName() {
		return name;
	}

	/**
	 * Changes the name of the patient to the given as parameter.
	 * @param name the name that will replace the patient's name
	 * @throws Exception Prevents that a blank or null name replace the patient's name.
	 */
	public void setName(String name) throws SoosEX {
		if (name == null) {
			throw new InvalidFieldEX("Nao foi possivel cadastrar o paciente. Nome do paciente nao pode ser null.");
		}
		if (name.trim().isEmpty()) {
			throw new InvalidFieldEX("Nao foi possivel cadastrar o paciente. Nome do paciente nao pode ser vazio.");
		}
		this.name = name;
	}
	
	/**
	 * Returns the String representation of the patient's age.
	 * @return the patient's age in a String form.
	 */
	public Integer getAge(){
		return age;
	}
	
	/**
	 * Changes the patient's age 
	 */
	public void setAge(){
		if(LocalDate.now().getDayOfYear() < birthDate.getDayOfYear()){
			this.age = LocalDate.now().getYear() - birthDate.getYear()-1;
		}else{
			this.age = (LocalDate.now().getYear() - birthDate.getYear());
		}
		
	}
	
	/**
	 * This method returns the birth date of the patient.
	 * @return the birth date of the patient.
	 */
	public LocalDate getBirthDate() {
		return birthDate;
	}
	
	/**
	 * This method format the given birth date to the date/month/year pattern.
	 * @param date the date to be formatted
	 * @return the formatted date.
	 */
	private LocalDate getDate(String date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate birth = LocalDate.parse(date, formatter);

		return birth;
	}
	
	/**
	 * Changes the birth date of the patient to the given as parameter.
	 * @param birthDate the date that will replace the patient's birth date.
	 * @throws DateTimeException prevents that a invalid date replace the patient's birth date.
	 */
	public void setBirthDate(LocalDate birthDate){
		this.birthDate = birthDate;
	}

	/**
	 * This method returns the weight of the patient.
	 * @return - a double containing the patient's weight.
	 */
	public Double getWeight() {
		return weight;
	}

	/**
	 * Changes the weight of the patient to the given as parameter.
	 * @param weight the weight that will replace the patient's weight.
	 * @throws Exception prevents that a invalid weight replace the patient's weight.
	 */
	public void setWeight(double weight) throws SoosEX {
		if (weight < 0.0) {
			throw new InvalidFieldEX("Nao foi possivel cadastrar o paciente. Peso do paciente nao pode ser negativo.");
		}
		this.weight = weight;
	}
	
	/**
	 * This method returns the blood type of the patient.
	 * @return a String containing the blood type of the patient.
	 */
	public String getBloodType() {
		return bloodType;
	}

	/**
	 * Changes the blood type of the patient to the given as parameter.
	 * @param bloodType the blood type that will replace the patient's blood type.
	 * @throws Exception prevents that a invalid blood type replace the patient's blood type.
	 */
	public void setBloodType(String bloodType) throws SoosEX {
		if (isBloodType(bloodType)) {
			this.bloodType = bloodType;
		} else {
			throw new InvalidFieldEX("Nao foi possivel cadastrar o paciente. Tipo sanguineo invalido.");
		}
	}
	
	/**
	 * This method checks if a blood type is valid or not.
	 * @param type blood type that will be checked.
	 * @return a boolean informing if the blood type is valid or not.
	 */
	private boolean isBloodType(String type) {
		BloodTypeEnum[] types = BloodTypeEnum.values();
		for (int i = 0; i < types.length; i++) {
			if (type.equalsIgnoreCase(types[i].getBloodType())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * This method returns the biological sex of the patient.
	 * @return a String containing the biological sex of the patient.
	 */
	public String getBioSex() {
		return bioSex;
	}
	
	/**
	 * Changes the biological sex to the given as parameter.
	 * @param bioSex the biological sex that will replace the patient's biological sex.
	 */
	public void setBioSex(String bioSex) {
		this.bioSex = bioSex;
	}
 
	/**
	 * Returns the gender of the patient.
	 * @return a String containing the patient's gender.
	 */
	public String getGender() {
		return gender;
	}
	
	/**
	 * Changes the patient's gender after the Sexual Reassignment procedure.
	 * @param newGender the patient's new gender
	 */
	public void setGenderProc(String newGender){
		this.gender = newGender;
	}

	/**
	 * Changes the gender of the patient to the given as parameter.
	 * @param gender the gender that will replace the patient's gender.
	 * @throws Exception prevents that a invalid gender will replace the patient's gender. 
	 */
	public void setGender(String gender) throws SoosEX {
		if (gender == null) {
			throw new InvalidFieldEX("Nao foi possivel cadastrar o paciente. Genero do paciente nao pode ser nulo.");
		}
		if (gender.trim().isEmpty()) {
			throw new InvalidFieldEX("Nao foi possivel cadastrar o paciente. Genero do paciente nao pode ser vazio.");
		}
		this.gender = gender;
	}
	
	/**
	 * Returns the patient's id.
	 * @return an int that represents the identification of the patient.
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Changes the patient's id to the given as parameter.
	 * @param id the number that will replace the patient's id.
	 */
	public void setId(String id){
		this.id = id;
	}

	/** 
	 * Returns the patient's fidelity card points.
	 * @return an int that represents the points of the patient.
	 */
	public int getPoints() {
		return this.fidelity.getPoints();
	}

	/** 
	 * Returns the patient's fidelity type.
	 * @return an String that represents the points of the patient.
	 */
	private String getFidelity() {
		return fidelity.toString();
	}
	
	/**
	 * Adds expenses to the patient.
	 * @param expenses to be added
	 * @throws InvalidFieldEX if the expenses parameter is negative
	 */
	public double calcExpenseDiscount(double expenses) throws InvalidFieldEX{
		return this.fidelity.calcExpenseDiscount(expenses);
	}
	
	/**
	 * Adds bonus credits to the patient, and change fidelity automaticaly, if possible.
	 * @param points to be added
	 * @throws InvalidFieldEX if the points parameter is negative
	 */
	public void addPoints(int points) throws InvalidFieldEX{
		this.fidelity.addPoints(points);
	}
	
	public String toString() {
		String name = "Paciente: " + this.name;
		String weightNbloodType = "Peso: " + this.weight + " kg Tipo Sanguineo: " + this.bloodType;
		String sexNgender = "Sexo: " + this.bioSex +" Genero: " + this.gender;
		return name + LINE_SEPARATOR + weightNbloodType + LINE_SEPARATOR + sexNgender; 
	}
	
}
