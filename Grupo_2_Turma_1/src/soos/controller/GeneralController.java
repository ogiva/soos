package soos.controller;


import java.io.Serializable;

import soos.exceptions.SoosEX;
import soos.exceptions.invalid.InvalidEX;
import soos.exceptions.invalid.InvalidFieldEX;
import soos.exceptions.invalid.InvalidKeyEX;
import soos.exceptions.invalid.InvalidNewDirectorEX;
import soos.exceptions.search.DiffRegistrationEX;
import soos.exceptions.search.ErrorSearchEX;
import soos.exceptions.session.AlreadyLaunchedEX;
import soos.exceptions.session.InexistentUserEX;
import soos.exceptions.session.PermissionDeniedEX;
import soos.exceptions.session.SessionEX;
import soos.exceptions.session.UserLoggedEX;
import soos.exceptions.session.WrongPswrdEX;
import soos.patient.MedicalRecord;

/**
 * The 'Controller' class implements the pattern Mediator/Controller.
 * The classes Employee, Director, Medic, Technician are composites.
 * 
 * @author Ogiva Team
 * 
 */
public class GeneralController implements Serializable{
	
	private static final String GENERAL_KEY = "c041ebf8";
	
	private final String REGIS_PATTERN_REGEX = "\\d+";
	
	private boolean unlockedSys;
	
	private ClinicalTeam employees;
	
	private Patients patients;
	
	private Pharmacy pharmacy;
	
	private OrganBank organBank;
	
	private Procedures procedure;
	
	
	/**
	 * Constructor method in class Controller.
	 * Initializes the attributes employees, 
	 * user logged, factory and the key to
	 * lock/unlock system.
	 * @param None
	 * @return void - constructs the instance Controller
	 */
	public GeneralController(){
				
		this.unlockedSys = false;
		
		this.employees = new ClinicalTeam();
		
		this.patients = new Patients();
		
		this.pharmacy = new Pharmacy();
		
		this.organBank = new OrganBank();
		
		this.procedure = new Procedures();
				
	}
	
	/**
	 * This method unlocks the system by changing the attribute that represents 
	 * the key to the unlocked system.
	 * Also creates the first employee and returns its registration.
	 * @param key  key to unlock the system.
	 * @param name  name of the first user created as the system is unlocked
	 * @param birth  birth date of the first user created as the system is unlocked
	 * @return String  registration of the first employee in the system
	 * @throws SoosEX If the system is already unlocked
	 */
	public String unlockSytem(String key, String name, String birth) throws SoosEX{
		// Unlock System neh?!
		if(this.unlockedSys == true){
			throw new AlreadyLaunchedEX();
		}else if(!checkKey(key)){
			throw new InvalidKeyEX();
		}
		unlockedSys = true;
		return employees.addUser(name, "Diretor Geral", birth);
		
	}
	/**
	 * This private method receives a key by input and compares it to the
	 * lock/unlock key of this system.
	 * @param key  key received by input
	 * @return boolean  true when the received key is equal to the system key, false when not
	 */
	private boolean checkKey(String key){
		
		if(key.equals(GENERAL_KEY)){
			return true;
		}else{
			return false;
		}
		
	}
	
	/**
	 * Method to log an user in the system.
	 * @param login  String representation of the registration of an employee
	 * @param password  String representation of the user's password
	 * @throws SessionEX If there is a user in session.
	 */
	public void login(String login, String password) throws SessionEX{
		
		employees.login(login, password);
	}
	
	/**
	 * This method logout an logged in user. Throws Exception if there is no logged user
	 * @param None
	 * @return void
	 * @throws ControllerEX If there is not a user in session.
	 */
	public void logout() throws UserLoggedEX{
		employees.logout();
	}
	/**
	 * This method is responsible by creating and adding an user.
	 * The creation is a delegation to the Factory class, who throws creational
	 * exceptions.
	 * @param name  name of the added user
	 * @param role  role of the added user (Director, Medic or Technician)
	 * @param birthDate  birth date of the added user
	 * @return String  the registration of the added user
	 * @throws SoosEX If the user is not valid in the system.
	 */
	public String addUser(String name, String role, String birthDate) throws SoosEX{
		
		if(!employees.checkDirector()){
			throw new PermissionDeniedEX("Erro no cadastro de funcionario. O funcionario "
					+employees.getName()+
						" nao tem permissao para cadastrar funcionarios.");
		}else if(role.equalsIgnoreCase("Diretor Geral")){
			throw new InvalidNewDirectorEX();
		}
		
		return employees.addUser(name, role, birthDate);
		
	}


	/**
	 * Method to check if the registration follows the pattern.
	 * @param regis  String representation of the registration of an employee.
	 * @return boolean  True if the registration follows the pattern, false else if.
	 */
	private boolean checkRegistration(String regis){
		return regis.matches(REGIS_PATTERN_REGEX);
	}
	
	/**
	 * Method to get informations about some user.
	 * @param regis  String representation of the registration of an employee.
	 * @param info  String representation of the information required.
	 * @return String  The required information.
	 * @throws SoosEX If the employee is not registered or the information required is not valid.
	 */
	public String getInfoFuncionario(String regis, String info) throws SoosEX{
		if(!employees.checkDirector()){
			throw new PermissionDeniedEX();
		}
		
		if(!checkRegistration(regis)){
			throw new DiffRegistrationEX();
		}
		if(employees.searchEmployee(regis) == false){
			throw new InexistentUserEX("Erro na consulta de funcionario. Funcionario nao cadastrado.");
		}
		
		return employees.getInfo(info, regis);
		
	}
		
	/**
	 * Method to update some information of some employee.
	 * @param regis  String representation of the registration of an employee.
	 * @param info  String representation of the information required.
	 * @param newInfo  String representation of the new value of the information.
	 * @throws SoosEX If the employee is not registered or the information is not valid.
	 */
	private void updateEmployeeInfo(String regis, String info, String newInfo) throws SoosEX{
		if(!employees.checkDirector()){
			throw new PermissionDeniedEX();
		}
		if(!checkRegistration(regis)){
			throw new DiffRegistrationEX();
		}
		if(employees.searchEmployee(regis) == false){
			throw new InexistentUserEX();
		}
		employees.updateInfo(info, regis, newInfo);
	}
	
	/**
	 * Method to update some information of the user in session.
	 * @param info  String representation of the information required.
	 * @param value  String representation of the new value of the information..
	 * @throws SoosEX If the information updated is not valid.
	 */
	private void updateOwnInfo(String info, String value) throws SoosEX{
		employees.updateInfo(info, employees.getRegistration(), value);
	}
	
	/**
	 * Method to change the password.
	 * @param oldPsswrd  String representation of the password of an employee.
	 * @param newPsswrd  String representation of the new password of an employee.
	 * @throws SoosEX If the new password is not valid or the old one is given wrong.
	 */
	public void changePassword(String oldPsswrd, String newPsswrd) throws SoosEX {
		employees.changePassword(oldPsswrd, newPsswrd);
	}
	
	/**
	 * Method to delete an employee from the system.
	 * @param regis  String representation of the registration of an employee.
	 * @param psswd  String representation of the password of the director.
	 * @throws SoosEX If the employee is not registered or the logged user does not have permission.
	 */
	public void delEmployee(String regis, String psswd) throws SoosEX{
		if(!employees.checkDirector()){
			throw new PermissionDeniedEX("Erro ao excluir funcionario. O funcionario " +employees.getName()+ 
					" nao tem permissao para excluir funcionarios.");
		}
		if(!regis.matches(REGIS_PATTERN_REGEX)){
			throw new DiffRegistrationEX("Erro ao excluir funcionario. A matricula nao segue o padrao.");
		}
		if(employees.searchEmployee(regis) == false){
			throw new InexistentUserEX("Erro ao excluir funcionario. Funcionario nao cadastrado.");
		}
		if(!employees.getPassword().equals(psswd)){
			throw new WrongPswrdEX("Erro ao excluir funcionario. Senha invalida.");
		}
		employees.delEmployee(regis);
	}
	
	/**
	 * Method to change some information from the employee.
	 * @param registration  String representation of the registration of an employee.
	 * @param info  String representation of the information required to change.
	 * @param value  String representation of the new value of the information.
	 * @throws SoosEX If the employee is not registered or the information is not valid.
	 */
	public void updateInfo(String registration, String info, String value) throws SoosEX{

		updateEmployeeInfo(registration, info, value);
		
	}
	
	/**
	 * Method to change some information from the employee. 
	 * @param info  String representation of the information required to change.
	 * @param value  String representation of the new value of the information.
	 * @throws SoosEX If the information is not valid.
	 */
	public void updateInfo(String info, String value) throws SoosEX{
		updateOwnInfo(info, value);
	}
	
	/**
	 * The method closes the system, usually used in the end of a frame.
	 * @throws ControllerEX If there is a user logged
	 */
	public void lockSystem() throws SoosEX{
		employees.isLogged();
		this.unlockedSys = false;
	}
	
	/**
	 * Method to add a new patient to the system.
	 * @param name  String representation of the name of a patient.
	 * @param birthDate  String representation of the birth of a patient.
	 * @param weight  double representation of the weight of a patient.
	 * @param bioSex  String representation of the biological sex of a patient.
	 * @param gender  String representation of the gender of a patient.
	 * @param bloodType  String representation of the blood type of a patient.
	 * @return The ID of the registered patient.
	 * @throws SoosEX If the patient is not valid in the system.
	 */
	public String addPatient(String name, String birthDate, double weight, String bioSex,
			String gender, String bloodType) throws SoosEX{
		
		if(!employees.checkTech()){
			throw new InvalidEX("Nao foi possivel cadastrar o paciente. O funcionario " +
					employees.getName()+" nao tem permissao para cadastrar pacientes.");
		}else if(patients.checkPatientExists(name, bloodType)){
			throw new InvalidEX("Nao foi possivel cadastrar o paciente. Paciente ja cadastrado.");
		}
		
		return patients.addPatient(name, birthDate, weight, bioSex, gender, bloodType);
		
	}	
	
	/**
	 * Method to get some information from a patient in the system.
	 * @param id  String representation of the ID of a patient.
	 * @param info  String representation of the information required.
	 * @return String  The information required.
	 * @throws SoosEX If the patient is not registered or the information is not valid.
	 */
	public String getInfoPaciente(String id, String info) throws SoosEX{
		
		if(patients.searchPatient(id) == -1){
			throw new InexistentUserEX();
		}
		
		return patients.getInfo(id, info);
	}
	
	/**
	 * Method to get the ID of the patient's Record
	 * @param id  String representation of the ID of a patient's record.
	 * @return The Record identification.
	 * @throws SoosEX If the record is not in the system.
	 */
	public String getRecord(int id) throws SoosEX{
		
		if(id < 0){
			throw new InvalidFieldEX("Erro ao consultar prontuario." +
					" Indice do prontuario nao pode ser negativo.");
		}else if (id >= patients.size()){
			throw new InvalidFieldEX("Erro ao consultar prontuario." +
					" Nao ha prontuarios suficientes (max = " +patients.size()+ ").");
		}
		return patients.getRecord(id);
	}
	
	/**
	 * Method to add a new medicine to the system.
	 * @param name  String representation of the name of a medicine.
	 * @param type  String representation of the type of a medicine.
	 * @param price  double representation of the price of a medicine.
	 * @param qnty  int representation of the quantity of a medicine.
	 * @param categories  String representation of the categories of a medicine.
	 * @return String  The name of a medicine.
	 * @throws SoosEX If the medicine is not valid in the system.
	 */
	public String addMedicine(String name, String type, double price, int qnty, String categories) throws SoosEX{
		if(!employees.checkTech()){
			throw new PermissionDeniedEX("Erro no cadastro de medicamento. O funcionario " +
					employees.getName()+ " nao tem permissao para cadastrar medicamentos.");
		}
		return pharmacy.addMedicine(name, type, price, qnty, categories);
	}
	
	/**
	 * Method to get some information from some medicine registered int the system.
	 * @param info The information required
	 * @param med The medicine which the information is taken.
	 * @return The required info.
	 * @throws SoosEX - If the medicine is not registered in the system or the information is not valid.
	 */
	public String getInfoMedicine(String info, String med) throws SoosEX{
		return pharmacy.getMedicineInfo(info, med);
	}
	
	/**
	 * Method to updated data from a medicine.
	 * @param medicine  String representation of the name of a medicine.
	 * @param info  String representation of the information required to change.
	 * @param newValue  String representation of the new value of the information
	 * @throws SoosEX If the medicine is not regitered in the system, or the information is not valid.
	 */
	public void updateMedicine(String medicine, String info, String newValue) throws SoosEX{
		pharmacy.updateMedicineInfo(medicine, info, newValue);
	}
	
	/**
	 * Method to search all medicines from a given category.
	 * @param category  String representation of the category of a medicine.
	 * @return String  The medicines with such category
	 * @throws SoosEX If the category is not valid in the system.
	 */
	public String searchByCategory(String category) throws SoosEX{
		return pharmacy.medicineSearchByCategory(category);
	}
	
	/**
	 * Method to search the toString from a medicine.
	 * @param name  String representation of the name of a medicine.
	 * @return String  The representation of the medicine data.
	 * @throws SoosEX If the medicine does not exists in the system.
	 */
	public String searchByName(String name) throws SoosEX{
		return pharmacy.medicineSearchByName(name);
	}
	
	/**
	 * Method to search medicines according to the given parameter.
	 * @param computer  Parameter to search the medicines.
	 * @return Name of all medicines sorted according to the computer.
	 * @throws SoosEX If an invalid sort is passed
	 */
	public String getPharmacy(String computer) throws SoosEX{
		return pharmacy.medicineSearchAll(computer);
	}
	
	/**
	 * Method to add a new organ to the organ bank.
	 * @param name  String representation of the name of an organ.
	 * @param bloodType  String representation of the blood type of the organ donator.
	 * @throws SoosEX If the organ is not valid in the system.
	 */
	public void addOrgan(String name, String bloodType) throws SoosEX{
		organBank.addOrgan(name, bloodType);
	}
	
	/**
	 * Method to remove an organ from the system.
	 * @param name  String representation of the name of an organ.
	 * @param bloodType  String representation of the blood type of an organ.
	 * @throws SoosEX If the organ does not exists.
	 */
	public void removeOrgan(String name, String bloodType) throws SoosEX{
		organBank.removeOrgan(name, bloodType);
	}
	
	/**
	 * Method to get how many organs with the given name there are in the system.
	 * @param name  String representation of the name of an organ.
	 * @return The number of organs with this name.
	 */
	public int searchOrganName(String name){
		return organBank.searchByName(name);
	}
	
	/**
	 * Method to check if the organ given as parameter exits in the system.
	 * @param name String representation of the name of an organ. 
	 * @param bloodType String representation of the blood type of an organ.
	 * @return True if the organ is registered, false else if.
	 * @throws SoosEX If the blood type is invalid.
	 */
	public boolean searchOrgan(String name, String bloodType) throws SoosEX{
		return organBank.search(name, bloodType);
	}
	
	/**
	 * Method to get the organs with the following blood type.
	 * @param bloodType  String representation of the blood type of an organ.
	 * @return The names of the organs with this blood type.
	 * @throws SoosEX When there are no organs registered with such BT
	 */
	public String searchOrganByBT(String bloodType) throws SoosEX{
		return organBank.searchByBT(bloodType);
	}
	
	/**
	 * Method to search organs with the following name.
	 * @param name  String representation of the name of an organ.
	 * @return  The blood types of a organ.
	 * @throws SoosEX If the organ is not registered in the system.
	 */
	public String searchOrganByBTName(String name) throws SoosEX{
		return organBank.searchBTByName(name);
	}
	
	/**
	 * Method to get the quantity of organs in the system.
	 * @return  The number of organs in the OB
	 */
	public int getQuantity(){
		return organBank.getQuantity();
	}
	
	/**
	 * Method to get the number of organs with the following name there are in the system.
	 * @param name  String representation of the name of an organ.
	 * @return The number of organs with such name.
	 * @throws SoosEX If the organ does is not registered in the system.
	 */
	public int getOrganQuantity(String name) throws SoosEX{
		return organBank.getOrganQuantity(name);
	}
	
	/**
	 * Method to get the ID of the following patient.
	 * @param name  String representation of the name of a patient.
	 * @return The ID of a patient
	 * @throws ErrorSearchEX If the patient does not exists.
	 */
	public String getPatientID(String name) throws ErrorSearchEX{
		return patients.getPatientID(name);
	}
	

	
	/**
	 * Method to perform a procedure in the following patient.
	 * @param procedure  String representation of the name of a procedure.
	 * @param name  String representation of the name of a patient.
	 * @param meds  String representation of the medicines required.
	 * @throws SoosEX If the procedure is not one of the valid procedures in the system, or the patient does not exists, or the pharmacy does not cantians the medicines.
	 */
	public void performProcedure(String procedure, String name, String meds) throws SoosEX{
		employees.checkMedic();
		MedicalRecord patient = patients.containsPatient(name);
		double medCost = pharmacy.containsMedicines(meds);
		this.procedure.performProcedure(employees.getName(), procedure, patient, medCost);
	}
	
	/**
	 * Method to perform a procedure in the following patient.
	 * @param procedure  String representation of the name of a procedure.
	 * @param name   String representation of the name of a patient.
	 * @throws SoosEX If the procedure is not one of the valid procedures in the system, or the patient does not exists.
	 */
	public void performProcedure(String procedure, String name) throws SoosEX{
		employees.checkMedic();
		MedicalRecord patient = patients.containsPatient(name);
		this.procedure.performProcedure(employees.getName(), procedure, patient);
	}
	
	/**
	 * Method to perform a procedure in the following patient.
	 * @param procedure - String representation of the name of a procedure.
	 * @param name  String representation of the name of a patient.
	 * @param meds  String representation of the medicines required.
	 * @param organs  String representation of the organs needed for the procedure.
	 * @throws SoosEX If the procedure is not one of the valid procedures in the system, or the patient does not exists, or the pharmacy does not cantians the medicines, or the organ bank does not contain the organs.
	 */
	public void performProcedure(String procedure, String name, String meds, String organs) throws SoosEX{
		employees.checkMedic();
		this.procedure.isProcedure(procedure);
		MedicalRecord patient = patients.containsPatient(name);
		double medCost = pharmacy.containsMedicines(meds);
		organBank.containsOrgan(organs, patient.getbloodType());
		this.procedure.performProcedure(employees.getName(), procedure, patient, medCost, organs);
	}
	
	/**
	 * Method to get the number of procedures in a patient.
	 * @param name  String representation of the name of a patient.
	 * @return
	 */
	public int getNumberOfProcedures(String name){
		return patients.getNoProcedures(name);
	}
	
	/**
	 * Method to get the number of points from a patient.
	 * @param name String representation of the name of a patient.
	 * @return The number os points.
	 * @throws InvalidFieldEX If the patient does not exists
	 */
	public int getPointsPatient(String name) throws InvalidFieldEX{
		return patients.getPoints(name);
	}
	
	/**
	 * Method to get all the expenses from a patient.
	 * @param patiente  String representation of the name of a patient.
	 * @return The total of expenses of such patient.
	 * @throws InvalidFieldEX If the patient does not exists.
	 */
	public double getExpensesPatient(String patient) throws InvalidFieldEX{
		return patients.getExpenses(patient);
	}
	
	/**
	 * Exports the record of the patients that has the id passed.
	 * @param id the id of the patient
	 * @throws SoosEX If the export had an error.
	 */
	public void exportPacient(String id) throws SoosEX {
		this.patients.exportPatientRecord(id);
	}
	
}