package soos.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import soos.exceptions.SoosEX;
import soos.exceptions.invalid.InvalidFieldEX;
import soos.exceptions.search.ErrorSearchEX;
import soos.exceptions.search.NoCategoryEX;
import soos.exceptions.update.UpdateFieldEX;
import soos.medicine.Medicine;
import soos.medicine.MedicineCategoryEnum;
import soos.medicine.MedicineComparatorByName;
import soos.medicine.MedicineFactory;

/**
 * The Pharmacy class represents a pharmacy that contains medicines, and can do some operations, such add, search medicine,
 * or anything else.
 * @author Ogiva Team
 *
 */
public class Pharmacy implements Serializable{
	
	private List<Medicine> pharmacy;
	private MedicineFactory factory;
	
	/**
	 * This method constructs a pharmacy, starting empty of medicines. 
	 */
	public Pharmacy() {
		this.pharmacy = new ArrayList<>();
		this.factory = new MedicineFactory();
	}
	
	/**
	 * Adds a medicine at the Pharmacy, with the attributes passed.
	 * @param name - the name of the Medicine
	 * @param type - the type of the Medicine
	 * @param generalPrice - the price
	 * @param quantity - the quantity to be added
	 * @param categories - the categories of the Medicine
	 * @return the name of the Medicine added
	 * @throws SoosEX - If an invalid attribute is passed
	 */
	public String addMedicine(String name, String type, double generalPrice, 
			int quantity, String categories) throws SoosEX{
		Medicine medicineCreated = this.factory.createMedicine(name, type, generalPrice,
				quantity, categories);
		this.pharmacy.add(medicineCreated);
		Collections.sort(pharmacy);
		return medicineCreated.getName();
	}
	
	private Medicine searchMedicine(String name){
		for(Medicine med : this.pharmacy){
			if(med.getName().equals(name)){
				return med;
			}
		}
		return null;
	}
	
	private String getMedicineType(String name){
		return this.searchMedicine(name).getType();
	}
	
	private String getCategory(String name){
		return this.searchMedicine(name).getCategoryString();
	}
	
	private double getMedicinePrice(String name){
		return this.searchMedicine(name).getPrice();
	}
	
	private void updateMedicinePrice(String name, double newPrice) throws SoosEX{
		this.searchMedicine(name).setPrice(newPrice);
	}
	
	private int getMedicineQuantity(String name){
		return this.searchMedicine(name).getQuantity();
	}
	
	private void updateMedicineQuantity(String name, int newQuantity) throws SoosEX{
		this.searchMedicine(name).setQuantity(newQuantity);;
	}
	
	private ArrayList<MedicineCategoryEnum> getMedicineCategory(String name){
		return this.searchMedicine(name).getCategory();
	}
	
	/**
	 * Return the Medicine information according to the request.
	 * @param info the requested information
	 * @param medicine the name of the medicine to be consulted
	 * @return a string that contains the Medicine information
	 * @throws SoosEX If an invalid request is passed
	 */
	public String getMedicineInfo(String info, String medicine) throws SoosEX{
		if(searchMedicine(medicine) == null){
			throw new ErrorSearchEX("Erro ao consultar medicamento. Medicamento nao cadastrado.");
		}
		if(info.equalsIgnoreCase("Tipo")){
			return getMedicineType(medicine);
		} else if(info.equalsIgnoreCase("Quantidade")){
			return String.valueOf(getMedicineQuantity(medicine));
		} else if(info.equalsIgnoreCase("Preco")){
			return String.valueOf(getMedicinePrice(medicine));
		} else if(info.equalsIgnoreCase("Categorias")){
			return getCategory(medicine);
		} else {
			throw new InvalidFieldEX("Erro ao consultar medicamento. Atributo invalido.");
		}
	}
	
	/**
	 * Update the Medicine information according to the request.
	 * @param medicine the name of the medicine to be updated
	 * @param info the information to be updated
	 * @param newValue the new value of the information update
	 * @throws SoosEX If an invalid attribute is passed
	 */
	public void updateMedicineInfo(String medicine, String info, String newValue) throws SoosEX{
		if(searchMedicine(medicine) == null){
			throw new ErrorSearchEX("Erro ao atualizar medicamento. Medicamento nao cadastrado.");
		}
		if(info.equalsIgnoreCase("Nome")){
			throw new UpdateFieldEX("Erro ao atualizar medicamento." 
					+" Nome do medicamento nao pode ser alterado.");
		} else if(info.equalsIgnoreCase("Tipo")){
			throw new UpdateFieldEX("Erro ao atualizar medicamento." 
					+" Tipo do medicamento nao pode ser alterado.");
		} else if(info.equalsIgnoreCase("Preco")){
			double newPrice = Double.parseDouble(newValue);
			updateMedicinePrice(medicine, newPrice);
			Collections.sort(pharmacy);
		}else if(info.equalsIgnoreCase("Quantidade")){
			int newQnt = Integer.parseInt(newValue);
			updateMedicineQuantity(medicine, newQnt);
		}
	}
	
	/**
	 * Search a medicine by name
	 * @param name the name of medicine
	 * @return the Medicine representation (information)
	 * @throws InvalidFieldEX If not exists any medicine with the name passed 
	 */
	public String medicineSearchByName(String name) throws InvalidFieldEX{
		if (searchMedicine(name) == null){
			throw new InvalidFieldEX("Erro na consulta de medicamentos. Medicamento nao cadastrado.");
		}
		return searchMedicine(name).toString();
	}
	
	/**
	 * Returns the medicines filtered by category.
	 * @param category the category filter
	 * @return a String that contains the medicines
	 * @throws SoosEX If an invalid category is passed
	 */
	public String medicineSearchByCategory(String category) throws SoosEX{
		isCategory(category);
		String medicines = "";
		for(Medicine med : this.pharmacy){
			if(containsCategory(med.getName(), category)){
				medicines = medicines + med.getName() + ",";
			}
		}
		if(medicines.equalsIgnoreCase("")){
			throw new ErrorSearchEX("Erro na consulta de medicamentos." 
					+" Nao ha remedios cadastrados nessa categoria.");
		}
		return medicines.substring(0, medicines.length()-1);
	}
	
	private boolean containsCategory(String name, String category){
		for(MedicineCategoryEnum cat : getMedicineCategory(name)){
			if(category.equals(cat.getCategory())){
				return true;
			}
		}
		return false;
	}
	
	private void isCategory(String category) throws SoosEX{
		if(!MedicineCategoryEnum.isCategory(category)){
			throw new NoCategoryEX("Erro na consulta de medicamentos. Categoria invalida.");
		}
	}
	
	/**
	 * Returns all the medicines filtered according to the request.
	 * @param computer the sort method of the medicines
	 * @return a String that contains all the medicines, sorted by the request
	 * @throws InvalidFieldEX If an invalid sort is passed
	 */
	public String medicineSearchAll(String computer) throws InvalidFieldEX{
		List<Medicine> medicines = new ArrayList<>(this.pharmacy);
		String allMedicines = "";
		if(computer.equalsIgnoreCase("alfabetica")){
			Collections.sort(medicines, new MedicineComparatorByName());
			for(Medicine med : medicines){
				allMedicines += med.getName() + ",";
			}
			return allMedicines.substring(0, allMedicines.length()-1);
		} else if(computer.equalsIgnoreCase("Preco")){
			for(Medicine med : medicines){
				allMedicines += med.getName() + ",";
			}
			return allMedicines.substring(0, allMedicines.length() - 1);
		} else{
			throw new InvalidFieldEX("Erro na consulta de medicamentos. Tipo de ordenacao invalida.");
		}
	}
	
	/**
	 * Checks if the pharmacy contains the medicines passed
	 * @param medicines a string with the medicines to be checked
	 * @return a double that represents the cost of the medicines
	 * @throws ErrorSearchEX If there's no medicine at the pharmacy
	 */
	public double containsMedicines(String medicines) throws ErrorSearchEX{
		if(medicines.trim().isEmpty()){
			throw new ErrorSearchEX("Erro na realizacao de procedimentos." 
					+" Nome do medicamento nao pode ser vazio.");
		}
		double cost = 0;
		String[] meds = medicines.split(",");
		List<String> medicinesList = Arrays.asList(meds);
		List<String> medicinesAdded = new ArrayList<>();
		for(String m: medicinesList){
			int counter = 0;
			for(Medicine med: pharmacy){
				if(med.getName().equalsIgnoreCase(m) && !medicinesAdded.contains(m)){
					medicinesAdded.add(m);
					counter = counter + 1;
					cost = cost + med.getPrice();
				}
			}
			if(counter == 0){
				throw new ErrorSearchEX("Erro na realizacao de procedimentos." 
						+" Medicamento nao cadastrado.");
			}

		}
		return cost;
	}
	
}
