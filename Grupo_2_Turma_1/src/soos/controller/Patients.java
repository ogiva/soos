package soos.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import soos.exceptions.SoosEX;
import soos.exceptions.invalid.InvalidFieldEX;
import soos.exceptions.search.ErrorSearchEX;
import soos.patient.MedicalRecord;

/**
 * This class represents the patients in the SOOS system.
 * @author Ogiva Team
 *
 */
public class Patients implements Serializable{
	
	private List<MedicalRecord> patients;

	public Patients(){
		this.patients = new ArrayList<>();
	}
	
	/**
	 * Method to add a new patient to the system.
	 * @param name  String representation of the name of a patient.
	 * @param birthDate  String representation of the birth date of a patient.
	 * @param weight  double representation of the weight of a patient.
	 * @param bioSex  String representation of the biological sex of a patient.
	 * @param gender  String representation of the gender of a patient.
	 * @param bloodType  String representation of the blood type of a patient.
	 * @return String  The ID of the new patient.
	 * @throws SoosEX If the patient is invalid in the system
	 */
	public String addPatient(String name, String birthDate, double weight, String bioSex,
			String gender, String bloodType) throws SoosEX{
	
		MedicalRecord pat = new MedicalRecord(name, birthDate, weight, bioSex, gender, bloodType, 
				generateID());
		patients.add(pat);
		Collections.sort(patients);
		return pat.getId();
		
		
	}
	
	/**
	 * Method to get an information from a patient.
	 * @param id  String representation of the ID of a patient.
	 * @param info  String representation of the required information.
	 * @return String  The value of the required information.
	 * @throws SoosEX If the patient does not exists in teh system or the information is invalid
	 */
	public String getInfo(String id, String info) throws SoosEX{
		
		MedicalRecord med = patients.get(searchPatient(id));
		if(info.equalsIgnoreCase("Nome")){
			return med.getName();
		}else if(info.equalsIgnoreCase("Data")){
			return med.getBirthDate().toString();
		}else if(info.equalsIgnoreCase("Sexo")){
			return med.getBioSex();
		}else if(info.equalsIgnoreCase("Genero")){
			return med.getGender();
		}else if(info.equalsIgnoreCase("TipoSanguineo")){
			return med.getbloodType();
		}else if(info.equalsIgnoreCase("Peso")){
			return med.getWeight().toString();
		}else if(info.equalsIgnoreCase("Idade")){
			return med.getAge().toString();
		}else{
			throw new InvalidFieldEX("Categoria inexistente");
		}
	}
	
	/**
	 * Method to search a patient in the system.
	 * @param patient  String representation of the ID of a patient.
	 * @return int  The index of the patient in the data structure of patients.
	 */
	public int searchPatient(String patient){
		for(MedicalRecord med: patients){
			if(med.getId().equals(patient)){
				return patients.indexOf(med);
			}
		}
		return -1;
	}
	
	/**
	 * Method to check if a patients is registered in the system.
	 * @param name  String representation of the name of a patient.
	 * @param blood  String representation of the Blood Type of a patient.
	 * @return boolean  True if the system already registered the patient, false else if.
	 */
	public boolean checkPatientExists(String name, String blood){
		
		for(MedicalRecord med: patients){
			if(med.getName().equals(name) && med.getbloodType().equals(blood)){
				return true;
			}
		}
		return false;
		
	}
	
	/**
	 * Method to get the number of patients registered.
	 * @return int  The number of patients in the system.
	 */
	public int size(){
		return patients.size();
	}
	
	/**
	 * Method to get the ID of a Record.
	 * @param id  int representation of the ID of a patient.
	 * @return String - The ID of the record.
	 */
	public String getRecord(int id){
		return patients.get(id).getId();
	}
	
	/**
	 * Methdo to get the ID from the name.
	 * @param name  String representation of the name of a patient.
	 * @return String  The ID of the patient.
	 * @throws ErrorSearchEX If the patient is not registered.
	 */
	public String getPatientID(String name) throws ErrorSearchEX{
		for(MedicalRecord med: patients){
			if(med.getName().equalsIgnoreCase(name)){
				return med.getId();
			}
		}
		throw new ErrorSearchEX("Paciente nao cadastrado.");
	}
	
	private String generateID(){
		return String.valueOf(patients.size());
	}

	/**
	 * Method to get the record of a patient to be used in the performance of a procedure.
	 * @param name  String representation of the ID of a patient.
	 * @return MedicalRecord  The Record of the patient. 
	 * @throws ErrorSearchEX If the ID is null or empty.
	 */
	public MedicalRecord containsPatient(String name) throws ErrorSearchEX{
		if(name.trim().isEmpty()){
			throw new ErrorSearchEX("Erro na realizacao de procedimentos." 
					+" ID do paciente nao pode ser vazio.");
		}
		for(MedicalRecord patient: patients){
			if(patient.getId().equalsIgnoreCase(name)){
				return patient;
			}
		}
		throw new ErrorSearchEX("");
		
	}
	
	/**
	 * Method to get the number of procedures that a patient has performed.
	 * @param name  String representation of the ID of a patient.
	 * @return int  The number of procedures from the patient.
	 */
	public int getNoProcedures(String name){
		return patients.get(searchPatient(name)).getNumberOfProcedures();
	}
	
	/**
	 * Method to get the total of points from a patient.
	 * @param name  String representation of the ID of a patient.
	 * @return The number of points from the patient.
	 * @throws InvalidFieldEX If the patient does not exists in the system.
	 */
	public int getPoints(String name) throws InvalidFieldEX{
		if(searchPatient(name) == -1){
			throw new InvalidFieldEX("");
		}
		return patients.get(searchPatient(name)).getPoints();
	}
	
	/**
	 * Method to get the expenses from a patient.
	 * @param name  String representation of the ID of a patient.
	 * @return double  The total of expenses.
	 * @throws InvalidFieldEX If the patient does not exists in the system.
	 */
	public double getExpenses(String name) throws InvalidFieldEX{
		if(searchPatient(name) == -1){
			throw new InvalidFieldEX("");
		}
		return patients.get(searchPatient(name)).getExpenses();
	}
	
	/**
	 * Exports the record of the patients that has the id passed.
	 * @param id the id of the patient
	 * @throws SoosEX If the export had an error.
	 */
	public void exportPatientRecord(String id) throws SoosEX{
		final File dir = new File("fichas_pacientes/");
		if(!dir.exists()){
			dir.mkdirs();
		}
		
		MedicalRecord patient = this.patients.get(searchPatient(id));
		File record = new File(dir, patient.getName() + "_" + LocalDate.now().toString() + ".txt");
		
		try {
			FileWriter writerRecord = new FileWriter(record);
			BufferedWriter bfRecord = new BufferedWriter(writerRecord);
			bfRecord.write(patient.toString());
			bfRecord.close();
		} catch (IOException e) {
			throw new SoosEX("Erro ao gerar ficha.");
		}
		
		
	}
	
}
