package soos.controller;

import java.io.Serializable;
import java.util.ArrayList;
import soos.exceptions.SoosEX;
import soos.exceptions.invalid.InvalidFieldEX;
import soos.exceptions.search.ErrorSearchEX;
import soos.organbank.Organ;
import soos.organbank.OrganFactory;
import soos.patient.BloodTypeEnum;

/**
 * The class Bank is an entity representing the Organs Bank of our system. 
 * The responsibilities is to manage the Collection of Organs, adding, removing,
 * searching Organs in it.
 * @author Ogiva Team
 *
 */
public class OrganBank implements Serializable{
	
	private ArrayList<Organ> bank;
	
	private OrganFactory factory;
	
	public OrganBank(){
		this.bank = new ArrayList<>();
		this.factory = new OrganFactory();
	}
	
	/**
	 * This method add creates a new Organ and then, if possible, adds it
	 * in the collection of Organs.
	 * @param name  String representation of the name of a organ
	 * @param bloodType  String representation of the donator's blood type
	 * @throws SoosEX If the organ is not valid.
	 */
	public void addOrgan(String name, String bloodType) throws SoosEX{
		Organ created = factory.createOrgan(name, bloodType);
		this.bank.add(created);
	}
	
	
	/**
	 * This method search the index of an Organ in the Collections of organ
	 * and then removes it from the Bank of Organs.
	 * @param name  String representation of the name of a Organ
	 * @param bloodType  String representation of the donator's blood type
	 * @throws SoosEX If the organs is not registered in the system.
	 */
	public void removeOrgan(String name, String bloodType) throws SoosEX{
		try{
			BloodTypeEnum.isBloodType(bloodType);
		}catch(SoosEX e){
			throw new InvalidFieldEX("Erro na retirada de orgaos. Tipo sanguineo invalido.");
		}
		
		int index = indexOrgan(name, bloodType);		
		bank.remove(index);
	}
	
	
	/**
	 * Private method to search the index of a Organ.
	 * @param name  String representation of the name of a Organ
	 * @param bloodType  String representation of the donator's blood type
	 * @return The index of a organ
	 * @throws ErrorSearchEX If there are no organs with such name and bt.
	 */
	private int indexOrgan(String name, String bloodType) throws ErrorSearchEX{
		for(Organ organ: bank){
			if(name.equalsIgnoreCase(organ.getName()) && bloodType.equalsIgnoreCase(organ.getBT())){
				return bank.indexOf(organ);
			}
		}
		throw new ErrorSearchEX("Erro na retirada de orgaos. Orgao nao cadastrado.");
	}
	
	
	/**
	 * This method search an Organ using the name and Blood Type.
	 * @param name  String representation of the name of a Organ
	 * @param bloodType  String representation of the donator's blood type
	 * @return boolean  true if there is an organ attending to the parameters, elsewhere false
	 * @throws SoosEX If the blood type is invalid.
	 */
	public boolean search(String name, String bloodType) throws SoosEX{
		BloodTypeEnum.isBloodType(bloodType);
		for(Organ organ: bank){
			if(name.equalsIgnoreCase(organ.getName()) && bloodType.equalsIgnoreCase(organ.getBT())){
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * This method search how many organs there are with the name passed as parameter.
	 * @param name  String representation of the name of a Organ
	 * @return The quantity of Organs in the bank
	 */
	public int searchByName(String name){
		
		int quantity = 0;
		for(Organ organ: bank){
			if(name.equalsIgnoreCase(organ.getName())){
				quantity = quantity + 1;
			}
		}
		return quantity;
		
	}
	
	/**
	 * This method returns the names of the organs that have the Blood Type passed as parameter
	 * throws exception if there are no organs registered with the following parameters.
	 * @param bloodType  String representation of the donator's blood type.
	 * @return The names of the organs with this blood type.
	 * @throws SoosEX When there are no organs registered with such BT
	 */
	public String searchByBT(String bloodType) throws SoosEX{
		BloodTypeEnum.isBloodType(bloodType);
		String search = "";
		for(Organ organ: bank){
			if(organ.getBT().equals(bloodType)){
				if(!search.contains(organ.getName())){
					search = search + organ.getName() + ",";
				}
			}
		}
		if(search.equalsIgnoreCase("")){
			throw new ErrorSearchEX("O banco de orgaos apresentou um erro." +
					" Nao ha orgaos cadastrados para esse tipo sanguineo.");
		}
		return search.substring(0, search.length()-1);
	}
	
	/**
	 * This method return the names of the organs with the following name.
	 * @param name  String representation of the name of an organ
	 * @return  The blood types of a organ.
	 * @throws SoosEX If the organ is not registered in the system.
	 */
	public String searchBTByName(String name) throws SoosEX{
		String search = "";
		for(Organ organ: bank){
			if(name.equalsIgnoreCase(organ.getName())){
				search = search + organ.getBT() + ",";
			}
		}
		if(search.equalsIgnoreCase("")){
			throw new ErrorSearchEX("O banco de orgaos apresentou um erro. Orgao nao cadastrado.");
		}
		return search.substring(0, search.length()-1);
	}
	
	/**
	 * This method return the number of organs with the following name.
	 * @param name  String representation of the name of an organ
	 * @return Number of organs with such name
	 * @throws SoosEX If there are no organs with the name
	 */
	public int getOrganQuantity(String name) throws SoosEX{
		int i = 0;
		for(Organ organ: bank){
			if(organ.getName().equalsIgnoreCase(name)){
				i = i + 1;
			}
		}
		if(i == 0){
			throw new ErrorSearchEX("O banco de orgaos apresentou um erro. Orgao nao cadastrado.");
		}
		return i;
	}
	
	/**
	 * This method return how many organs there are in the bank
	 * @return The number of organs in the OB
	 */
	public int getQuantity(){
		return bank.size();
	}
	
	public void containsOrgan(String organ, String bloodtype) throws ErrorSearchEX{
		if(organ.trim().isEmpty()){
			throw new ErrorSearchEX("Erro na realizacao de procedimentos." 
					+" Nome do orgao nao pode ser vazio.");
		}
		
		int counter = 0;
		for(Organ organs: bank){
			if(organs.getName().equalsIgnoreCase(organ) && organs.getBT().equalsIgnoreCase(bloodtype)){
				counter = counter + 1;
			}
		}
		if(counter == 0){
			throw new ErrorSearchEX("Erro na realizacao de procedimentos." 
					+" Banco nao possui o orgao especificado.");
		}
		
	}

}
