package soos.controller;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.HashMap;

import soos.employee.Employee;
import soos.employee.EmployeeFactory;
import soos.employee.Medic;
import soos.employee.RoleEnum;
import soos.exceptions.SoosEX;
import soos.exceptions.session.CloseUserLoggedEX;
import soos.exceptions.session.InexistentUserEX;
import soos.exceptions.session.LockedFieldEX;
import soos.exceptions.session.PermissionDeniedEX;
import soos.exceptions.session.SessionEX;
import soos.exceptions.session.UserLoggedEX;
import soos.exceptions.session.WrongPswrdEX;

/**
 * Hash table based. This implementation provides 
 * some of the optional map operations. This collections represents the clinical body
 * in the SOOS. Perform all actions related to employees.
 * 
 * @author Ogiva Team
 *
 */
public class ClinicalTeam implements Serializable{
	
	private HashMap<String, Employee> employees;
	
	private EmployeeFactory factory;
	
	private Employee logged;
	
	public ClinicalTeam(){
		this.employees = new HashMap<>();
		this.factory = new EmployeeFactory();
	}
	
	/**
	 * Method to add a new employee to the system.
	 * @param name  String representation of the name of an employee.
	 * @param role  String representation of the role of an employee.
	 * @param birthDate  String representation of the birth date of an employee.
	 * @return String  The registration of the new employee/user
	 * @throws SoosEX If the user data is invalid
	 */
	public String addUser(String name, String role, String birthDate) throws SoosEX{
		
		Employee created = factory.createEmployee(name, birthDate, role, getIDRegister());
		
		this.employees.put(created.getRegistration(), created);
		
		return created.getRegistration();
		
	}
	
	/**
	 * Methdo to search and return an employee using its login/registration
	 * @param login  String representation of the registration of an employee.
	 * @return 
	 */
	private Employee login(String login){
		return employees.get(login);
	}
	
	/**
	 * Method to search an employee in the system.
	 * @param registration  String representation of the registration of an employee.
	 * @return boolean  True if the employee is registered in the system, false else if.
	 */
	public boolean searchEmployee(String registration){
		return employees.containsKey(registration);
	}
	
	/**
	 * Method to check if a given password is correct.
	 * @param login  String representation of the registration of an employee.
	 * @param password  String representation of the password given.
	 * @throws SessionEX If the password is incorrect.
	 */
	public void checkPswrd(String login, String password) throws SessionEX{
		if(!employees.get(login).getPassword().equals(password)){
			throw new WrongPswrdEX();
		}
	}
	
	/**
	 * Method to get some information from an employee.
	 * @param info  String representation of the information wanted
	 * @param regis  String representation of the registration of an employee.
	 * @return String  The wanted information
	 * @throws SessionEX If the paramet is not valid.
	 */
	public String getInfo(String info, String regis) throws SessionEX{
		if(info.equalsIgnoreCase("Nome")){
			return employees.get(regis).getName();
		}else if(info.equalsIgnoreCase("Cargo")){
			return employees.get(regis).getRole().getPlace();
		}else if(info.equalsIgnoreCase("Data")){
			return employees.get(regis).getBirthDate().toString();
		}else{
			throw new LockedFieldEX();
		}
	}
	
	/**
	 * Method to update an information of an employee. 
	 * @param info  String representation of the information required to change
	 * @param regis  String representation of the registration of an employee.
	 * @param newInfo  String representation of the new value to the information
	 * @throws SoosEX If the information required is not visible
	 */
	public void updateInfo(String info, String regis, String newInfo) throws SoosEX{
		if(info.equalsIgnoreCase("Nome")){
			employees.get(regis).changeName(newInfo);
		}else if(info.equalsIgnoreCase("Data")){
			employees.get(regis).changeDate(newInfo);
		}else{
			throw new LockedFieldEX();
		}
	}
	
	/**
	 * Method to delete an employee.
	 * @param regis  String representation of the registration of an employee.
	 */
	public void delEmployee(String regis){
		this.employees.remove(regis);
	}
	
	/**
	 * Private method to get the id of an employee to register it.
	 * @return String  The ID of a registration.
	 */
	private String getIDRegister(){
		NumberFormat formatter = new DecimalFormat("000");
		
		String number = formatter.format(this.employees.size() + 1);
		
		return number;
		
	}
	
	public void changePassword(String oldPsswrd, String newPsswrd) throws SoosEX{
		if(logged == null){
			throw new UserLoggedEX();	
		}
		this.logged.changePassword(oldPsswrd, newPsswrd);
	}
	
	public void login(String login, String password) throws SessionEX{
		if(!(this.logged == null)){
			throw new UserLoggedEX("Nao foi possivel realizar o login." 
					+" Um funcionario ainda esta logado: " +getName()+ ".");
		}
		if(!searchEmployee(login)){
			throw new InexistentUserEX();
		}else{
			checkPswrd(login, password);
		}
		//Logged in sucessfully
		this.logged = login(login);
		
	}
	
	public void logout() throws UserLoggedEX{
		if(this.logged == null){
			throw new UserLoggedEX("Nao foi possivel realizar o logout. Nao ha um funcionario logado.");
		}
		
		this.logged = null;
		
	}
	
	/**
	 * The method checks if the logged user is a Director,
	 * there are functionalities that are not allowed if the logged user is not
	 * a Director
	 * @param None
	 * @return boolean True if the user is Director, false if not
	 */
	public boolean checkDirector(){
		
		if(this.logged.getRole().equals(RoleEnum.DIRECTOR)){
			return true;
		}
		return false;
		
	}
	
	/**
	 * Method to check if the user in session is a medic.
	 * @throws PermissionDeniedEX
	 */
	public void checkMedic() throws PermissionDeniedEX{
		if(!(this.logged.getClass() == Medic.class)){
			throw new PermissionDeniedEX("Erro na realizacao de procedimentos." 
					+" O funcionario " +getName()+ " nao tem permissao para realizar procedimentos.");
		}
	}
	
	/**
	 * Method to check if the user in session is a technician.
	 * @return
	 */
	public boolean checkTech(){
		if(this.logged.getRole().equals(RoleEnum.TECHNICIAN)){
			return true;
		}
		return false;
	}
	
	public void isLogged() throws SoosEX{
		if(!(logged == null)){
			throw new CloseUserLoggedEX("Nao foi possivel fechar o sistema."+
					" Um funcionario ainda esta logado: "+getName()+".");
		}
	}
	

	/**
	 * Method to get the name of the user in session.
	 * @return String
	 */
	public String getName() {
		return logged.getName();
	}

	/**
	 * Method to get the registration of the user in session.
	 * @return String
	 */
	public String getRegistration() {
		return logged.getRegistration();
	}

	/**
	 * Method to get the password of the user in session.
	 * @return String
	 */
	public String getPassword() {
		return logged.getPassword();
	}

	/**
	 * Method to get the birth of the user in session.
	 * @return LocalDate
	 */
	public LocalDate getBirthDate() {
		return logged.getBirthDate();
	}

	/**
	 * Method to get the role of the user in session.
	 * @return RoleEnum
	 */
	public RoleEnum getRole() {
		return logged.getRole();
	}
	

}
