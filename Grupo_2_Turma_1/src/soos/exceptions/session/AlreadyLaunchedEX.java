package soos.exceptions.session;

public class AlreadyLaunchedEX extends SessionEX{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1194498450663408722L;

	public AlreadyLaunchedEX() {
		super("Erro ao liberar o sistema. Sistema liberado anteriormente.");
	}
	
	public AlreadyLaunchedEX(String msg) {
		super(msg);
	}

}
