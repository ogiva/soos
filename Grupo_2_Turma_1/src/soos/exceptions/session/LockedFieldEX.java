package soos.exceptions.session;

public class LockedFieldEX extends SessionEX{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2224614858520212837L;

	public LockedFieldEX() {
		super("Erro na consulta de funcionario. A senha do funcionario eh protegida.");
	}
	
	public LockedFieldEX(String msg) {
		super(msg);
	}
	
}
