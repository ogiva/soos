package soos.exceptions.session;

public class WrongPswrdEX extends SessionEX{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1603410701077005471L;

	public WrongPswrdEX(String msg) {
		super(msg);
	}
	
	public WrongPswrdEX() {
		super("Nao foi possivel realizar o login. Senha incorreta.");
	}
}
