package soos.exceptions.session;

public class InexistentUserEX extends SessionEX{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3246821692623530528L;
	
	public InexistentUserEX() {
		super("Nao foi possivel realizar o login. Funcionario nao cadastrado.");
	}
	
	public InexistentUserEX(String msg) {
		super(msg);
	}
	

}
