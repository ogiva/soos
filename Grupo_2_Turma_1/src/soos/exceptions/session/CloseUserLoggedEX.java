package soos.exceptions.session;

public class CloseUserLoggedEX extends SessionEX{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3098848668702018246L;
	
	public CloseUserLoggedEX(String msg){
		super(msg);
	}
	
	public CloseUserLoggedEX(){
		super("Nao foi possivel fechar o sistema. Um funcionario ainda esta logado.");
	}
	
}
