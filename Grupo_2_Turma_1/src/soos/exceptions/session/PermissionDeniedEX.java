package soos.exceptions.session;

public class PermissionDeniedEX extends SessionEX{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7696859334623393564L;
	
	public PermissionDeniedEX(String msg){
		super(msg);
	}
	
	public PermissionDeniedEX(){
		super("Erro na operacao. O funcionario nao tem permissao para realizar operacao.");
	}

}
