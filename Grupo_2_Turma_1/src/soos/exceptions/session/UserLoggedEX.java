package soos.exceptions.session;

public class UserLoggedEX extends SessionEX{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3161838042296830302L;
	
	public UserLoggedEX(){
		super("Nao foi possivel realizar operacao. Um funcionario ainda esta logado.");
	}
	
	public UserLoggedEX(String msg){
		super(msg);
	}
	
}
