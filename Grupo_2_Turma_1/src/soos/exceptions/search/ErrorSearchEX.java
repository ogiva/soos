package soos.exceptions.search;

import soos.exceptions.SoosEX;

public class ErrorSearchEX extends SoosEX{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6441176281267442711L;

	public ErrorSearchEX(String msg) {
		super(msg);
	}

}
