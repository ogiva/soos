package soos.exceptions.search;

public class NoCategoryEX extends ErrorSearchEX{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2897640026017952384L;

	public NoCategoryEX(String msg) {
		super(msg);
	}
	
	public NoCategoryEX() {
		super("Erro na consulta. Nao ha elementos cadastrados nessa categoria.");
	}
	
}
