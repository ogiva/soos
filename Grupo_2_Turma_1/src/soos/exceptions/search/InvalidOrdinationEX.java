package soos.exceptions.search;

public class InvalidOrdinationEX extends ErrorSearchEX{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2976452170537927545L;

	public InvalidOrdinationEX() {
		super("Erro na consulta de medicamentos. Tipo de ordenacao invalida.");
	}
	
	public InvalidOrdinationEX(String msg) {
		super(msg);
	}
	
}
