package soos.exceptions.search;

public class DiffRegistrationEX extends ErrorSearchEX{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9045296945173244180L;

	public DiffRegistrationEX(String msg) {
		super(msg);
	}
	
	public DiffRegistrationEX() {
		super("Erro na consulta de funcionario. A matricula nao segue o padrao.");
	}
}
