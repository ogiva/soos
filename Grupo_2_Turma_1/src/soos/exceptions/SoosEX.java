package soos.exceptions;

public class SoosEX extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3143748524851994072L;
	
	public SoosEX(String msg) {
		super(msg);
	}
	
}
