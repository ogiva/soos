package soos.exceptions.update;

public class UpdateNRegisteredEX extends UpdateInfoEX{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4256085062135957113L;

	public UpdateNRegisteredEX() {
		super("Erro ao atualizar. Elemento nao cadastrado.");
	}
	
	public UpdateNRegisteredEX(String msg) {
		super(msg);
	}
	
}
