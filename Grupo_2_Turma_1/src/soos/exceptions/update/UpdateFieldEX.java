package soos.exceptions.update;

public class UpdateFieldEX extends UpdateInfoEX {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1543890476828611441L;

	public UpdateFieldEX() {
		super("Erro ao atualizar atributo. Atributo nao pode ser alterado.");
	}
	
	public UpdateFieldEX(String msg) {
		super(msg);
	}
	
}
