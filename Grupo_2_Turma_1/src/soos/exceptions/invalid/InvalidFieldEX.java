package soos.exceptions.invalid;

public class InvalidFieldEX extends InvalidEX{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7489220238605263677L;

	public InvalidFieldEX(){
		super("Erro na operacao. Campo invalido.");
	}
	
	public InvalidFieldEX(String msg){
		super(msg);
	}
	
}
