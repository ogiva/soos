package soos.exceptions.invalid;

public class InvalidNewDirectorEX extends InvalidEX{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8512848507476591457L;

	public InvalidNewDirectorEX(String msg) {
		super(msg);
	}
	
	public InvalidNewDirectorEX() {
		super("Erro no cadastro de funcionario. Nao eh possivel criar mais de um Diretor Geral.");
	}

}
