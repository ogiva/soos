package soos.exceptions.invalid;

public class InvalidKeyEX extends InvalidEX{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1134314248077783153L;

	public InvalidKeyEX() {
		super("Erro ao liberar o sistema. Chave invalida.");
	}
	
	public InvalidKeyEX(String msg) {
		super(msg);
	}
	
}
