package soos.exceptions.invalid;

import soos.exceptions.SoosEX;

public class InvalidEX extends SoosEX{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2616319683194307810L;

	public InvalidEX(String msg) {
		super(msg);
	}
	
}
