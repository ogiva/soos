package soos.procedures;

import java.time.LocalDate;
import java.io.Serializable;

import soos.exceptions.SoosEX;
import soos.patient.MedicalRecord;

/**
 * This class represents the Bariatic Surgery procedure.
 * It implements the ProcedureIF interface.
 * @author Ogiva Team
 */
public class BariatricSurgery implements ProcedureIF, Serializable {
	
	private final String LINE_SEPARATOR = System.lineSeparator();
	
	private static final double PROCEDURE_PRICE = 7600;
	private static final int PROCEDURE_POINTS = 100;
	private static final double PERCENTAGE_OF_LOSS_WEIGHT = 10;
	private MedicalRecord patient;
	private LocalDate date;
	private String medic;
	
	/**
	 * The constructor receives a MedicalRecord as a parameter
	 * @param patient: MedicalRecord
	 */
	public BariatricSurgery (String medic, MedicalRecord patient) {
		this.patient = patient;
		this.date = LocalDate.now();
		this.medic = medic;
	}
	
	/**
	 * Method that performs the procedure at the patient.
	 * It reduces the patient's weith at 10%
	 */
	public void performProcedure() throws SoosEX {
//		this.patient.registerProcedure();
		double newWeight = patient.getWeight() - (this.patient.getWeight() * PERCENTAGE_OF_LOSS_WEIGHT) / 100;
		this.patient.setWeight(newWeight);
	}
	
	/**
	 * Method that informs the procedure's price
	 */
	public double getProcedurePrice() {
		return this.PROCEDURE_PRICE;
	}
	
	/**
	 * Method that informs the procedure's fidelity points
	 */
	public int getProcedurePoints() {
		return this.PROCEDURE_POINTS;
	}

	public String getDate() {
		return this.date.toString();
	}

	public String getMedic() {
		return this.medic;
	}
	
	public String toString() {
		String surgery = "--> Cirurgia Bariatrica:";
		String details = "....... Data: " + this.getDate() + " Medico: " + this.getMedic();
		return surgery + LINE_SEPARATOR + details;
	}
}
