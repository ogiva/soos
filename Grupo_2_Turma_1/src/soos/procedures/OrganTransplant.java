package soos.procedures;

import java.time.LocalDate;
import java.io.Serializable;

import soos.exceptions.SoosEX;
import soos.patient.MedicalRecord;

/**
 * This class represents the Organ Transplant procedure.
 * It implements the ProcedureIF interface.
 * @author Ogiva Team
 */
public class OrganTransplant implements ProcedureIF, Serializable {
	
	private final String LINE_SEPARATOR = System.lineSeparator();
	
	private static final double PROCEDURE_PRICE = 12500;
	private static final int PROCEDURE_POINTS = 160;
	private MedicalRecord patient;
	private LocalDate date;
	private String medic;
	private String organs;
	
	/**
	 * The constructor receives a MedicalRecord as a parameter
	 * @param patient: MedicalRecord
	 */
	public OrganTransplant (String medic, MedicalRecord patient, String organs){
		this.patient = patient;
		this.date = LocalDate.now();
		this.medic = medic;
		this.organs = organs;
	}
	/**
	 * Method that performs the procedure at the patient.
	 * It does not change any parameter of the patient.
	 */
	public void performProcedure() throws SoosEX {
		// do nothing
		//this.patient.registerProcedure();
	}
	
	/**
	 * Method that informs the procedure's price
	 */
	public double getProcedurePrice() {
		return this.PROCEDURE_PRICE;
	}
	
	/**
	 * Method that informs the procedure's fidelity points
	 */
	public int getProcedurePoints() {
		return this.PROCEDURE_POINTS;
	}
	public String getDate() {
		return this.date.toString();
	}
	public String getMedic() {
		return this.medic;
	}
	
	public String toString() {
		String transplant = "--> Consulta clinica:";
		String details = "....... Data: " + this.getDate() + " Medico: " + this.getMedic();
		String organ = "....... Orgao transplantado: " + this.organs;
		return transplant + LINE_SEPARATOR + details;
	}

}
