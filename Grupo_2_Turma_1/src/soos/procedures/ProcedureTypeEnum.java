package soos.procedures;

import java.io.Serializable;

/**
 * This enum enumerates all the possible type of procedures. Containing the attributes: name.
 * @author Ogiva Team
 *
 */
public enum ProcedureTypeEnum implements Serializable{
	
	MEDICAL_APPOINTMENT("Consulta clinica"),
	BARIATIC_SURGERY("Cirurgia bariatrica"),
	SEXUAL_REASSIGNMENT("Redesignacao sexual"),
	ORGAN_TRANSPLANT("Transplante de orgaos");
	
	String procedure;
	
	/**
	 * Constructor method receives a String of the name of a procedure.
	 * @param procedure
	 */
	ProcedureTypeEnum(String procedure) {
		this.procedure = procedure;
	}
	
	/**
	 * Method to return the String representation of the name of a procedure.
	 * @return The name of the procedure.
	 */
	public String getProcedureName() {
		return this.procedure;
	}
	
}
