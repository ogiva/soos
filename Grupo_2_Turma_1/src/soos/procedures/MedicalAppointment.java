package soos.procedures;

import java.time.LocalDate;
import java.io.Serializable;

import soos.exceptions.SoosEX;
import soos.patient.MedicalRecord;

/**
 * This class represents the Medical Appointment procedure.
 * It implements the ProcedureIF interface.
 * @author Ogiva Team
 */
public class MedicalAppointment implements ProcedureIF, Serializable {
	
	private final String LINE_SEPARATOR = System.lineSeparator();
	
	private static final double PROCEDURE_PRICE = 350;
	private static final int PROCEDURE_POINTS = 50;
	private MedicalRecord patient;
	private LocalDate date;
	private String medic;
	
	/**
	 * The constructor receives a MedicalRecord as a parameter
	 * @param patient: MedicalRecord
	 */
	public MedicalAppointment (String medic, MedicalRecord patient) {
		this.patient = patient;
		this.date = LocalDate.now();
		this.medic = medic;
	}
	
	/**
	 * Method that performs the procedure at the patient.
	 * It does not change any parameter of the patient.
	 */
	public void performProcedure() throws SoosEX {
		// do nothing! just register
//		this.patient.registerProcedure();
//		this.patient.addExpense(PROCEDURE_PRICE);
//		this.patient.addPoints(PROCEDURE_POINTS);
	}
	
	/**
	 * Method that informs the procedure's price
	 */
	public double getProcedurePrice() {
		return this.PROCEDURE_PRICE;
	}
	
	/**
	 * Method that informs the procedure's fidelity points
	 */
	public int getProcedurePoints() {
		return this.PROCEDURE_POINTS;
	}

	public String getDate() {
		return this.date.toString();
	}

	public String getMedic() {
		return this.medic;
	}

	@Override
	public String toString() {
		String appointment = "--> Consulta clinica:";
		String details = "....... Data: " + this.getDate() + " Medico: " + this.getMedic();
		return appointment + LINE_SEPARATOR + details;
	}
	
}
