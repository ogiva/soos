package soos.procedures;

import soos.exceptions.SoosEX;

/**
 * Inteface that defines the type Procedure
 * @author Ogiva Team
 */
public interface ProcedureIF {
	
	/**
	 * Method that performs the procedure at the patient.
	 */
	public void performProcedure() throws SoosEX;
	
	/**
	 * Method that informs the procedure's price
	 * @return
	 */
	public double getProcedurePrice();
	
	/**
	 * Method that informs the procedure's fidelity points
	 * @return
	 */
	public int getProcedurePoints();
	
	public String getDate();
	
	public String getMedic();

}
