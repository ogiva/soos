package soos.procedures;

import java.io.Serializable;

import soos.exceptions.SoosEX;
import soos.exceptions.invalid.InvalidFieldEX;
import soos.patient.MedicalRecord;

/**
 * The "ProcedureC" class is responsible for creating an object of the type
 * Procedure.
 * 
 * @author Ogiva Team
 */
public class ProcedureFactory implements Serializable{

	/**
	 * This method creates a procedure according with the given parameters
	 * 
	 * @param procedureName
	 *            - the procedure's name
	 * @param patient
	 *            - the medical record of the patient where the procedure will
	 *            be realized.
	 * @return - an object of the type Procedure
	 * @throws SoosEX
	 *             - Ensures that the procedure will not be created with invalid
	 *             information.
	 */
	public ProcedureIF createProcedure(String medic, String procedureName, MedicalRecord patient, String organs) throws SoosEX {
		ProcedureIF procedure = null;
		if (procedureName == null) {
			throw new InvalidFieldEX(
					"Erro na realizacao de procedimentos. " + "Nome do procedimento nao pode ser nulo.");
		}
		if (procedureName.trim().isEmpty()) {
			throw new InvalidFieldEX(
					"Erro na realizacao de procedimentos. " + "Nome do procedimento nao pode ser vazio.");
		}
		if (!isProcedure(procedureName)) {
			throw new InvalidFieldEX("Erro na realizacao de procedimentos. " + "Procedimento invalido.");
		}
		if (procedureName.equalsIgnoreCase(ProcedureTypeEnum.BARIATIC_SURGERY.getProcedureName())) {
			procedure = this.createBariatricSurgery(medic, patient);
		} else if (procedureName.equalsIgnoreCase(ProcedureTypeEnum.MEDICAL_APPOINTMENT.getProcedureName())) {
			procedure = this.createMedicalAppointment(medic, patient);
		} else if (procedureName.equalsIgnoreCase(ProcedureTypeEnum.SEXUAL_REASSIGNMENT.getProcedureName())) {
			procedure = this.createSexualReassignment(medic, patient);
		}else{
			procedure = this.createOrganTransplant(medic, patient, organs);
		}
		return procedure;
	}
	
	/**
	 * Checks if the procedure is valid.
	 * @param procedureName - procedure that will be checked.
	 * @return - a boolean informing if the procedure is valid or not.
	 */
	public boolean isProcedure(String procedureName) {
		ProcedureTypeEnum procedures[] = ProcedureTypeEnum.values();
		for (int i = 0; i < procedures.length; i++) {
			if (procedureName.equalsIgnoreCase(procedures[i].getProcedureName())) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Create an procedure of the BariatricSurgery type.
	 * @param patient - the medical record of the patient where the procedure will be realized.
	 * @return - an object of the type Procedure.
	 */
	private ProcedureIF createBariatricSurgery(String medic, MedicalRecord patient) {
		return new BariatricSurgery(medic, patient);
	}
	
	/**
	 * Create an procedure of the SexualReassignment type.
	 * @param patient - the medical record of the patient where the procedure will be realized.
	 * @return - an object of the type Procedure.
	 */
	private ProcedureIF createSexualReassignment(String medic, MedicalRecord patient) {
		return new SexualReassignment(medic, patient);
	}
	
	/**
	 * Create an procedure of the MedicalAppointment type.
	 * @param patient - the medical record of the patient where the procedure will be realized.
	 * @return - an object of the type Procedure.
	 */
	private ProcedureIF createMedicalAppointment(String medic, MedicalRecord patient) {
		return new MedicalAppointment(medic, patient);
	}
	
	/**
	 * Create an procedure of the OrganTransplant type.
	 * @param patient - the medical record of the patient where the procedure will be realized.
	 * @return - an object of the type Procedure.
	 */
	private ProcedureIF createOrganTransplant(String medic, MedicalRecord patient, String organs) throws SoosEX {
		return new OrganTransplant(medic, patient, organs);
	}
}
