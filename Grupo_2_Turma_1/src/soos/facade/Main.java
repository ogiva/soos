package soos.facade;

import easyaccept.EasyAccept;

public class Main {
	
	public static void main(String[] args) {
		args = new String[] {"soos.facade.Facade", "acceptancetests/usecase_1.txt",  
				"acceptancetests/usecase_2.txt", "acceptancetests/usecase_3.txt",
				"acceptancetests/usecase_4.txt", "acceptancetests/usecase_5.txt",
				"acceptancetests/usecase_6.txt", "acceptancetests/usecase_7.txt"};
		EasyAccept.main(args);
	}

}
