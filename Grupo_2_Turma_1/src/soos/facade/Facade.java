package soos.facade;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.format.DateTimeParseException;

import soos.controller.GeneralController;
import soos.exceptions.SoosEX;
import soos.exceptions.invalid.InvalidFieldEX;
import soos.exceptions.session.SessionEX;
import soos.exceptions.session.UserLoggedEX;


public class Facade {
	
	private GeneralController controller;
	
	private File soos;
	
	public Facade() {
		soos = new File("system_data/soos.dat");
	}
	
	public void iniciaSistema(){
		try{
			if(!soos.exists()){
				this.controller = new GeneralController();
			}else{
				FileInputStream file = new FileInputStream(soos);
				ObjectInputStream input = new ObjectInputStream(file);
				controller = (GeneralController)input.readObject();
				input.close();
			}
		}catch(Exception e){
			System.out.println("deu merda de novo");
			e.printStackTrace();
		}
	}
	
	public String liberaSistema(String chave, String nome, String dataNascimento)throws SoosEX {
		
		return controller.unlockSytem(chave, nome, dataNascimento);
		
	}
	
	public void login(String matricula, String senha)throws SessionEX{
		controller.login(matricula, senha);
	}
	
	public String cadastraFuncionario(String nome, String cargo, String data)  throws SoosEX {
		return controller.addUser(nome, cargo, data);		
	}
	
	public void logout() throws UserLoggedEX {
		controller.logout();
	}
	
	public void fechaSistema() throws SoosEX{
		controller.lockSystem();
		try{
			if(!soos.exists()){
				soos.createNewFile();
			}
			FileOutputStream out = new FileOutputStream(soos);
			ObjectOutputStream output = new ObjectOutputStream(out);
			output.writeObject(controller);
			output.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public String getInfoFuncionario(String matricula, String atributo) throws SoosEX{
		return controller.getInfoFuncionario(matricula, atributo);
	}
	
	public void excluiFuncionario(String matricula, String senha) throws SoosEX{
		controller.delEmployee(matricula, senha);
	}
	
	public void atualizaInfoFuncionario(String matricula, String atributo, String valor) throws SoosEX{
		controller.updateInfo(matricula, atributo, valor);
	}
	
	public void atualizaInfoFuncionario(String atributo, String valor) throws SoosEX{
		controller.updateInfo(atributo, valor);
	}
	
	public void atualizaSenha(String senhaAntiga, String novaSenha) throws SoosEX{
		controller.changePassword(senhaAntiga, novaSenha);
	}
	
	public String cadastraPaciente(String Nome, String Data, double Peso,
			String Sexo, String Genero, String tipoSanguineo) throws SoosEX{
		
		try{
			return controller.addPatient(Nome, Data, Peso, Sexo, Genero, tipoSanguineo).toString();
		}catch(DateTimeParseException e){
			throw new InvalidFieldEX("Nao foi possivel cadastrar o paciente. Data invalida.");
		}
		
	}	
	
	public String getInfoPaciente(String paciente, String atributo) throws SoosEX{
		return controller.getInfoPaciente(paciente, atributo);
	}
	
	public String getProntuario(int posicao) throws SoosEX{
		return controller.getRecord(posicao).toString();
	}	
	
	public String cadastraMedicamento(String nome, String tipo, double preco, int quantidade, String categorias) throws SoosEX{
		return controller.addMedicine(nome, tipo, preco, quantidade, categorias);
	}
	
	public String getInfoMedicamento(String atributo, String medicamento) throws SoosEX{
		return controller.getInfoMedicine(atributo, medicamento);
	}
	
	public void atualizaMedicamento(String nome, String atributo, String novoValor) throws SoosEX{
		controller.updateMedicine(nome, atributo, novoValor);
	}
	
	public String consultaMedCategoria(String categoria) throws SoosEX{
		return controller.searchByCategory(categoria);
	}
	
	public String consultaMedNome(String nomeRemedio) throws SoosEX{
		return controller.searchByName(nomeRemedio);
	}
	
	public String getEstoqueFarmacia(String ordenacao) throws SoosEX{
		return controller.getPharmacy(ordenacao);
	}
	
	public void cadastraOrgao(String nome, String tipoSanguineo) throws SoosEX{
		controller.addOrgan(nome, tipoSanguineo);
	}
	
	public String buscaOrgPorSangue(String tipoSanguineo) throws SoosEX{
		return controller.searchOrganByBT(tipoSanguineo);
	}
	
	public String buscaOrgPorNome(String nome) throws SoosEX{
		return controller.searchOrganByBTName(nome);
	}
	
	public boolean buscaOrgao(String nome, String tipoSanguineo) throws SoosEX{
		return controller.searchOrgan(nome, tipoSanguineo);
	}
	
	public void retiraOrgao(String nome, String tipoSanguineo) throws SoosEX{
		controller.removeOrgan(nome, tipoSanguineo);
	}
	
	public int totalOrgaosDisponiveis(){
		return controller.getQuantity();
	}
	
	public int qtdOrgaos(String nome) throws SoosEX{
		return controller.getOrganQuantity(nome);
	}
	
	public String getPacienteID(String nome) throws SoosEX{
		return controller.getPatientID(nome);
	}
	
	public void realizaProcedimento(String procedimento, String nomePaciente, String medicamentos) throws SoosEX{
		controller.performProcedure(procedimento, nomePaciente, medicamentos);
	}
	
	public void realizaProcedimento(String procedimento, String nomePaciente) throws SoosEX{
		controller.performProcedure(procedimento, nomePaciente);
	}
	
	public void realizaProcedimento(String procedimento, String nomePaciente,String orgao, String medicamentos) throws SoosEX{
		controller.performProcedure(procedimento, nomePaciente, medicamentos, orgao);
	}
	
	public int getTotalProcedimento(String nomePaciente){
		return controller.getNumberOfProcedures(nomePaciente);
	}
	
	public int getPontosFidelidade(String paciente) throws SoosEX{
		return controller.getPointsPatient(paciente);
	}
	
	public double getGastosPaciente(String paciente) throws SoosEX{
		return controller.getExpensesPatient(paciente);
	}
	
	public void exportarFichaPaciente(String id) throws Exception{
		controller.exportPacient(id);
	}
}
