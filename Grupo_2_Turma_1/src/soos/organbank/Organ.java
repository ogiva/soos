package soos.organbank;

import java.io.Serializable;

import soos.exceptions.SoosEX;
import soos.exceptions.invalid.InvalidFieldEX;
import soos.patient.BloodTypeEnum;


/**
 * This class implements the Interface Comparator. It is a representation of a Organ,
 * containing name and the Blood Type of the donator. 
 * @author Ogiva Team
 *
 */
public class Organ implements Comparable<Organ>, Serializable{
	
	private String name;
	
	private BloodTypeEnum donatorBT;
	
	
	/**
	 * Constructor specified by the name of the organ and the blood type.
	 * @param name  String representation of the name of a Organ
	 * @param donatorBT  String representation of the donator's blood type
	 * @throws SoosEX If the name of the organ is null or empty.
	 */
	public Organ(String name, BloodTypeEnum donatorBT) throws SoosEX{
		setName(name);
		this.donatorBT = donatorBT;
	}
	
	/**
	 * This method sets the name of the organ to the one passed as parameter.
	 * @param name  String representation of the name of a Organ
	 * @throws InvalidFieldEX If the name of the organ is null or empty.
	 */
	private void setName(String name) throws InvalidFieldEX{
		if(name.trim().isEmpty()){
			throw new InvalidFieldEX("O banco de orgaos apresentou um erro. Nome do orgao nao pode ser vazio.");
		}
		this.name = name;
	}
	
	
	/**
	 * This method return the String representation of the name of the Organ.
	 * @return String  The String representation of this Organ
	 */
	public String getName(){
		return this.name;
	}
	
	/**
	 * This method return the String representation of the donator's blood type of the Organ.
	 * @return The blood type of the organ.
	 */
	public String getBT(){
		return this.donatorBT.getBloodType();
	}

	@Override
	public int compareTo(Organ o) {
		return this.getName().compareTo(o.getName());
	}

}
