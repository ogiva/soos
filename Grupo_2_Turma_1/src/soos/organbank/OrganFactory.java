package soos.organbank;

import java.io.Serializable;

import soos.exceptions.SoosEX;
import soos.exceptions.invalid.InvalidFieldEX;
import soos.patient.BloodTypeEnum;


/**
 * This class implements the Factory Pattern. It creates and returns a new Organ
 * @author Ogiva Team
 *
 */
public class OrganFactory implements Serializable{
	
	/**
	 * This method is used to create and return a Organ with the passed parameters
	 * @param name  String representation of the name of a Organ
	 * @param bloodType  String representation of the donator's blood type
	 * @return Organ  The created organ
	 * @throws SoosEX If the organ is not valid in the system.
	 */
	public Organ createOrgan(String name, String bloodType) throws SoosEX{
		return new Organ(name, getBloodType(bloodType));
	}
	
	
	/**
	 * This private method return the BloodTypeEnum representation of the donator's blood Type.
	 * @param bt  String representation of the donator's blood type
	 * @return BloodTypeEnum  BloodTypeEnum representation of the donator's blood type
	 * @throws InvalidFieldEX If the blood type is not valid.
	 */
	private BloodTypeEnum getBloodType(String bt) throws InvalidFieldEX{
		if(bt.equalsIgnoreCase(BloodTypeEnum.A_NEGATIVE.getBloodType())){
			return BloodTypeEnum.A_NEGATIVE;
		}else if(bt.equalsIgnoreCase(BloodTypeEnum.A_POSITIVE.getBloodType())){
			return BloodTypeEnum.A_POSITIVE;
		}else if(bt.equalsIgnoreCase(BloodTypeEnum.AB_NEGATIVE.getBloodType())){
			return BloodTypeEnum.AB_NEGATIVE;
		}else if(bt.equalsIgnoreCase(BloodTypeEnum.AB_POSITIVE.getBloodType())){
			return BloodTypeEnum.AB_POSITIVE;
		}else if(bt.equalsIgnoreCase(BloodTypeEnum.B_NEGATIVE.getBloodType())){
			return BloodTypeEnum.B_NEGATIVE;
		}else if(bt.equalsIgnoreCase(BloodTypeEnum.B_POSITIVE.getBloodType())){
			return BloodTypeEnum.B_POSITIVE;
		}else if(bt.equalsIgnoreCase(BloodTypeEnum.O_NEGATIVE.getBloodType())){
			return BloodTypeEnum.O_NEGATIVE;
		}else if(bt.equalsIgnoreCase(BloodTypeEnum.O_POSITIVE.getBloodType())){
			return BloodTypeEnum.O_POSITIVE;
		}else{
			throw new InvalidFieldEX("O banco de orgaos apresentou um erro. Tipo sanguineo invalido.");
		}
	}

}
