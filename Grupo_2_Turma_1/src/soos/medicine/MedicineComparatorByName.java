package soos.medicine;

import java.io.Serializable;
import java.util.Comparator;

public class MedicineComparatorByName implements Comparator<Medicine>, Serializable{

	@Override
	public int compare(Medicine med1, Medicine med2) {
		return med1.getName().compareTo(med2.getName());
	}
	
}