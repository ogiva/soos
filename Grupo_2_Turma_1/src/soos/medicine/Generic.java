package soos.medicine;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import soos.exceptions.SoosEX;
import soos.exceptions.invalid.InvalidFieldEX;

/**
 * Class that represents the generic medicine type. This class extends the Medicine class.
 * @author Ogiva Team
 */
public class Generic extends Medicine implements Serializable{
	
	private static final double DESCONTO = 0.4;
	
	private final String TYPE = "Generico";
	
	/**
	 * The constructor of the class receives the following parameters:
	 * @param name: String with the medicine's name
	 * @param price: Double with the medicine's price
	 * @param quantity: Integer with the available amount of medicine
	 * @param categories: List with the categories of the medicine
	 * @throws SoosEX
	 */
	public Generic(String name, double price, int quantity, 
			ArrayList<MedicineCategoryEnum> categories) throws SoosEX {
		super(name, price, quantity, categories);
	}
	
	/**
	 * Method that sets the price of the medication
	 * @param price: Double with the price of the medication
	 */
	public void setPrice(double price) throws SoosEX {
		if (price < 0){
			throw new InvalidFieldEX
			("Erro no cadastro de medicamento. Preco do medicamento nao pode ser negativo.");
		}
		double newPrice = price - (price * DESCONTO);
		BigDecimal bd = new BigDecimal(newPrice).setScale(2, RoundingMode.HALF_EVEN);
		super.price = bd.doubleValue();
	}
	
	/**
	 * Method that returns the type of the medicine
	 * @return String with the type of the medicine
	 */
	public String getType() {
		return TYPE;
	}

}
