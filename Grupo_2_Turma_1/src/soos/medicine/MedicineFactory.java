package soos.medicine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import soos.exceptions.SoosEX;
import soos.exceptions.invalid.InvalidFieldEX;

/**
 * The MedicineFactory class is responsible for creating an object of the type Medicine.
 * @author Ogiva Team
 *
 */
public class MedicineFactory implements Serializable{
	
	/**
	 * This method creates a Medicine according to the given parameters
	 * @param name - the Medicine's name
	 * @param type - the Medicine's type
	 * @param generalPrice - the Medicine's general price
	 * @param quantity - the Medicine's quantity
	 * @param categories - the Medicine's categories
	 * @return an object of the type Medicine
	 * @throws SoosEX - If an invalid attribute is passed
	 */
	public Medicine createMedicine(String name, String type, double generalPrice, int quantity,
			String categories) throws SoosEX {
		
		if(type.equalsIgnoreCase("Referencia")){
			return createRef(name, generalPrice, quantity, categories);
		}else if(type.equalsIgnoreCase("Generico")){
			return createGeneric(name, generalPrice, quantity, categories);
		}else{
			
		}
		return null;
	}

	/**
	 * This method creates a Generic Medicine according to the given parameters
	 * @param name - the Medicine's name
	 * @param generalPrice - the Medicine's general price
	 * @param quantity - the Medicine's quantity
	 * @param categories - the Medicine's categories
	 * @return an object of the type Medicine
	 * @throws SoosEX - If an invalid attribute is passed
	 */
	private Medicine createGeneric(String name, double generalPrice, int quantity,
			String categories) throws SoosEX {
		
		ArrayList<MedicineCategoryEnum> categorieSet = new ArrayList<MedicineCategoryEnum>();
		for(String word: Arrays.asList(categories.split(", "))){
			categorieSet.add(checkCategory(word));
		}
		Collections.sort(categorieSet);
		return new Generic(name, generalPrice, quantity, categorieSet);
		
	}
	
	/**
	 * This method creates a Reference Medicine according to the given parameters
	 * @param name - the Medicine's name
	 * @param generalPrice - the Medicine's general price
	 * @param quantity - the Medicine's quantity
	 * @param categories - the Medicine's categories
	 * @return an object of the type Medicine
	 * @throws SoosEX - If an invalid attribute is passed
	 */
	private Medicine createRef(String name, double generalPrice, int quantity,
			String categories) throws SoosEX {
		
		ArrayList<MedicineCategoryEnum> categorieSet = new ArrayList<MedicineCategoryEnum>();
		for(String word: Arrays.asList(categories.split(","))){
			categorieSet.add(checkCategory(word));
		}
		Collections.sort(categorieSet);
		return new RefMedicine(name, generalPrice, quantity, categorieSet);
		
	}
	
	/**
	 * This method check if the category passed is valid
	 * @param category - the categoriy to be checked
	 * @return an enum of the type MedicineCategoryEnum
	 * @throws SoosEX - If the category isn't valid
	 */
	private MedicineCategoryEnum checkCategory(String category) throws SoosEX{
		if(category.equalsIgnoreCase(MedicineCategoryEnum.ANTI_INFLAMMATORY.getCategory())){
			return MedicineCategoryEnum.ANTI_INFLAMMATORY;
		}else if(category.equalsIgnoreCase(MedicineCategoryEnum.ANTIBIOTIC.getCategory())){
			return MedicineCategoryEnum.ANTIBIOTIC;
		}else if(category.equalsIgnoreCase(MedicineCategoryEnum.ANTIEMETIC.getCategory())){
			return MedicineCategoryEnum.ANTIEMETIC;
		}else if(category.equalsIgnoreCase(MedicineCategoryEnum.ANTIPYRETIC.getCategory())){
			return MedicineCategoryEnum.ANTIPYRETIC;
		}else if(category.equalsIgnoreCase(MedicineCategoryEnum.HORMONE.getCategory())){
			return MedicineCategoryEnum.HORMONE;
		}else if(category.equalsIgnoreCase(MedicineCategoryEnum.PAINKILLER.getCategory())){
			return MedicineCategoryEnum.PAINKILLER;
		}else{
			throw new InvalidFieldEX("Erro no cadastro de medicamento. Nome do medicamento nao pode ser vazio.");
		}
	}
	
	
}
