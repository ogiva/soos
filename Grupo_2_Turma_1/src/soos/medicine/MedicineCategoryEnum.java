package soos.medicine;

import java.io.Serializable;

/**
 * This enum enumerates all the types of medicines in the system. Containing the string representing the name.
 * @author Ogiva Team
 *
 */
public enum MedicineCategoryEnum implements Serializable{
	
	PAINKILLER("analgesico"),
	ANTIBIOTIC("antibiotico"),
	ANTIEMETIC("antiemetico"),
	ANTI_INFLAMMATORY("antiinflamatorio"),
	ANTIPYRETIC("antitermico"),
	HORMONE("hormonal");
		
	private final String category;
	
	private MedicineCategoryEnum(String category){
		this.category = category;
	}

	public String getCategory(){
		return this.category;
	}
	
	/**
	 * Method to check if the String received as parameter is one of the medicines types.
	 * @param category String representing the medicine type.
	 * @return A boolean, true if is one of the medicine types, false else if.
	 */
	public static boolean isCategory(String category){
		if(category.equalsIgnoreCase(PAINKILLER.getCategory())){
			return true;
		}else if(category.equalsIgnoreCase(ANTI_INFLAMMATORY.getCategory())){
			return true;
		}else if(category.equalsIgnoreCase(ANTIBIOTIC.getCategory())){
			return true;
		}else if(category.equalsIgnoreCase(ANTIEMETIC.getCategory())){
			return true;
		}else if(category.equalsIgnoreCase(ANTIPYRETIC.getCategory())){
			return true;
		}else if(category.equalsIgnoreCase(HORMONE.getCategory())){
			return true;
		}else{
			return false;
		}
	}
	
}