package soos.medicine;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import soos.exceptions.SoosEX;
import soos.exceptions.invalid.InvalidFieldEX;

/**
 * This class defines the Medicine type. It implements Comparable.
 * @author Ogiva Team
 */
public abstract class Medicine implements Comparable<Medicine>, Serializable{
	
	private String name;
	protected double price;
	private int quantity;
	private ArrayList<MedicineCategoryEnum> categories;
	
	/**
	 * The constructor of the class receives the following parameters:
	 * @param name: String with the medicine's name
	 * @param price: Double with the medicine's price
	 * @param quantity: Integer with the available amount of medicine
	 * @param categories: List with the categories of the medicine
	 * @throws SoosEX
	 */
	public Medicine (String name, double price, int quantity,
			ArrayList<MedicineCategoryEnum> categories) throws SoosEX {
		
		setName(name);
		setPrice(price);
		setQuantity(quantity);
		this.categories = categories;
		
	}
	
	/**
	 * Method that returns the medicine's name
	 * @return String with the medicine's name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Method that sets the name of the medicine
	 * @param name: String with the name of the medicine
	 * @throws SoosEX
	 */
	public void setName(String name) throws SoosEX {
		if (name == null || name.trim().isEmpty()) {
			throw new InvalidFieldEX 
			("Erro no cadastro de medicamento. Nome do medicamento nao pode ser vazio.");
		}
		this.name = name;
	}

	/**
	 * Method that informs the price of the medicine
	 * @return Double with the medicine's price
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * Abstract method that defines the price of a medicine.
	 * @param price: Double
	 * @throws SoosEX
	 */
	public abstract void setPrice(double price) throws SoosEX;
	
	/**
	 * Abstract method that defines getting a medicine type
	 * @return String
	 */
	public abstract String getType();

	/**
	 * Method that informs the available quantity of the medicine
	 * @return Integer
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * Method that sets the amount of medicine available
	 * @param quantity: Integer
	 * @throws SoosEX
	 */
	public void setQuantity(int quantity) throws SoosEX {
		if (quantity < 0) {
			throw new InvalidFieldEX
			("Erro no cadastro de medicamento. Quantidade do medicamento nao pode ser negativo.");
		}
		this.quantity = quantity;
	}

	/**
	 * Method that informs the categories of the medicine
	 * @return ArrayList<MedicineCategoryEnum>
	 */
	public ArrayList<MedicineCategoryEnum> getCategory(){
		return categories;
	}
	
	/**
	 * Method that returns a String with the categories of the medicine.
	 * @return String
	 */
	public String getCategoryString() {
		String cats = "";
		int i = 0;
		for(MedicineCategoryEnum medicine: categories){
			cats = cats + medicine.getCategory();
			i += 1;
			if(i < categories.size()){
				cats = cats + ",";
			}
		}
		return cats;
	}

	/**
	 * Method that sets the categories of the medicine
	 * @param category: ArrayList<MedicineCategoryEnum>
	 */
	public void setCategory(ArrayList<MedicineCategoryEnum> category) throws SoosEX {
		if(category == null) {
			throw new SoosEX("Erro no cadastro do medicamento. Categoria não pode ser vazia.");
		}
		this.categories = category;
	}
	
	/**
	 * Method that compares a medicine to another.
	 * It compares by price of the medicines.
	 */
	public int compareTo(Medicine med2) {
		if (this.getPrice() > med2.getPrice()) {
			return 1;
		} else if (this.getPrice() < med2.getPrice()) {
			return -1;
		} else {
			return 0;
		}
	}
	
	public String toString() {
		//Medicamento de Referencia: Hioscina - Preco: R$ 10,00 - Disponivel: 300 - Categorias: antiemetico
		NumberFormat nf = new DecimalFormat("#0.00", new DecimalFormatSymbols(new Locale("pt", "BR")));
		String about = "Medicamento " +getType()+ ": " +getName()+ " - Preco: R$ " +nf.format(getPrice())+
				" - Disponivel: "
					+getQuantity()+ " - Categorias: " +getCategoryString();
		return about;
	}
}
