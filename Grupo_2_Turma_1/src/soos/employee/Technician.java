package soos.employee;

import java.io.Serializable;
import java.time.LocalDate;

import soos.exceptions.invalid.InvalidFieldEX;

/**
 * Class that represents the real type Technician. This class extends the class Employee
 * @author Ogiva Team
 */
public class Technician extends Employee implements Serializable{
	
	/**
	 * Constructor method. It receives the following parameters:
	 * @param name: String with the Technician's name
	 * @param birthdate: String with the Technician's birth date
	 * @param id: String with the employee's unique ID
	 * @throws InvalidFieldEX
	 */
	public Technician (String name, String birthdate, String id) throws InvalidFieldEX{
		super(name, birthdate, RoleEnum.TECHNICIAN, id);
	}
	
	public String generateRegistration(String id){
		String registration = "";
		registration = registration + this.getRole().getId();
		
		registration = registration +  LocalDate.now().getYear();
		
		registration = registration + id;
		
		this.setRegistration(registration);
		
		return registration;
	}

}
