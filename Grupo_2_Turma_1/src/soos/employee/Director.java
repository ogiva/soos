package soos.employee;

import java.io.Serializable;
import java.time.LocalDate;

import soos.exceptions.invalid.InvalidFieldEX;

/**
 * The "Director" class implements the employee type Director, containing it's information
 * and it's own methods. It's an inherited class from the super-class "Employee"
 * @author Ogiva Team
 */

public class Director extends Employee implements Serializable{
	
	public Director(String name, String birthdate, String id) throws InvalidFieldEX {
		super(name, birthdate, RoleEnum.DIRECTOR, id);
	}
	
	/**
	 * @param registration - unique code that identifies an employee
	 * @param password - acess code of the user
	 */
	public Director() {
		super(RoleEnum.DIRECTOR);
	}
	
	/**
	 * Method that creats a unique code for an employee.
	 */
	public String generateRegistration(String id){
		String registration = "";
		registration = registration + this.getRole().getId();
		
		registration = registration +  LocalDate.now().getYear();
		
		registration = registration + id;
		
		this.setRegistration(registration);
		
		return registration;
	}

} 