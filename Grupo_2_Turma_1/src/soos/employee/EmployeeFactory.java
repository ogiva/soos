package soos.employee;

import java.io.Serializable;

import soos.exceptions.invalid.InvalidFieldEX;

/**
 * The classe EmployeeFactory implements the Factory design pattern that has resposability to criate Employees.
 * @author Ogiva Team
 *
 */
public class EmployeeFactory implements Serializable{
	
	/**
	 * This method creates an Employee according to the given parameters and to the given role, 
	 * and return it.
	 * @param name Employee's name
	 * @param birthDate Employee's birth date
	 * @param role Employee's role
	 * @return the created Employee
	 * @throws InvalidFieldEX
	 */
	public Employee createEmployee(String name, String birthDate, String role, String id) throws InvalidFieldEX{
		if(role.equalsIgnoreCase((RoleEnum.DIRECTOR.getPlace()))){
			return this.createDirector(name, birthDate, id);
		} else if(role.equalsIgnoreCase((RoleEnum.MEDIC.getPlace()))){
			return this.createMedic(name, birthDate, id);
		} else if(role.equalsIgnoreCase((RoleEnum.TECHNICIAN.getPlace()))){
			return this.createTechnician(name, birthDate, id);
		} else if(role.trim().isEmpty()){
			throw new InvalidFieldEX("Erro no cadastro de funcionario. Nome do cargo nao pode ser vazio.");
		}else{
			throw new InvalidFieldEX("Erro no cadastro de funcionario. Cargo invalido.");
		}
	}
	
	/**
	 * This method creates an Director according to the given parameters, and return it.
	 * @param name Director's name
	 * @param birthDate Director's birth date
	 * @return the created Director
	 * @throws InvalidFieldEX 
	 */
	private Director createDirector(String name, String birthDate, String id) throws InvalidFieldEX{
		return new Director(name, birthDate, id);
	}
	
	/**
	 * This method creates an Medic according to the given parameters, and return it.
	 * @param name Medic's name
	 * @param birthDate Medic's birth date
	 * @return the created Medic
	 * @throws InvalidFieldEX 
	 */
	private Medic createMedic(String name, String birthDate, String id) throws InvalidFieldEX{
		return new Medic(name, birthDate, id);
	}
	
	/**
	 * This method creates an Technician according to the given parameters, and return it.
	 * @param name Technician's name
	 * @param birthDate Technician's birth date
	 * @return the created Technician
	 * @throws InvalidFieldEX 
	 */
	private Technician createTechnician(String name, String birthDate, String id) throws InvalidFieldEX{
		return new Technician(name, birthDate, id);
	}
}	
