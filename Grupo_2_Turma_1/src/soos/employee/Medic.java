package soos.employee;

import java.io.Serializable;
import java.time.LocalDate;

import soos.exceptions.invalid.InvalidFieldEX;

/**
 * Class that represents the real type Medic. This class extends the class Employee
 * @author Ogiva Team
 */
public class Medic extends Employee implements Serializable{
	
	/**
	 * Constructor method. It receives the following parameters:
	 * @param name: String with the Medic's name
	 * @param birthdate: String with the Medic's birth date
	 * @param id: String with the employee's unique ID
	 * @throws InvalidFieldEX
	 */
	public Medic(String name, String birthdate, String id) throws InvalidFieldEX {
		super(name, birthdate, RoleEnum.MEDIC, id);
	}
	
	/**
	 * Method that generates a registration id for the employee
	 */
	public String generateRegistration(String id){
		String registration = "";
		registration = registration + this.getRole().getId();
		
		registration = registration +  LocalDate.now().getYear();
		
		registration = registration + id;
		
		this.setRegistration(registration);
		
		return registration;
	}

}
