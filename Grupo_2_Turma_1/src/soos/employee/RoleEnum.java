package soos.employee;

import java.io.Serializable;

/**
 * The class RoleEnum contains the constants that represents the possible roles of the employees.
 * @author Ogiva Team
 *
 */
public enum RoleEnum implements Serializable{
	
	DIRECTOR("1", "Diretor Geral"), 
	MEDIC("2", "Medico"),
	TECHNICIAN("3", "Tecnico Administrativo");
	
	private final String id;
	private final String role;
	
	private RoleEnum(String id, String role){
		this.id = id;
		this.role = role;
	}
	
	/**
	 * Returns the role's id.
	 * @return a String that represents the role's id
	 */
	public String getId(){
		return this.id;
	}
	
	/**
	 * Returns the role full name.
	 * @return a string that represents the role full name 
	 */
	public String getPlace(){
		return this.role;
	}

}
