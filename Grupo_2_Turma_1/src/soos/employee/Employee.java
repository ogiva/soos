package soos.employee;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import soos.exceptions.invalid.InvalidFieldEX;
import soos.exceptions.session.WrongPswrdEX;

/**
 * Abstract class that represents the Employee type.
 * The class contains the name, registration, password, birth date
 * and the role of the employee.
 * @author Ogiva Team
 */
public abstract class Employee implements Serializable{

	
	private final String NAME_PATTERN_REGEX = "[a-zA-Z]{1,16}";
	private final String SENHA_PATTERN_REGEX = "\\w{8,12}";
	
	private String name;
	private String registration;
	private String password;
	private LocalDate birthDate;
	private RoleEnum role;
	
	/**
	 * The following parameters are needed to create an employee:
	 * @param name: String.
	 * @param birthDate: String
	 * @param role: RoleEnum
	 * @param id: String
	 * @throws InvalidFieldEX
	 */
	public Employee(String name, String birthDate, RoleEnum role, String id) throws InvalidFieldEX {
		this.setName(name);
		this.birthDate = getDate(birthDate);
		this.setRole(role);
		generateRegistration(id);
		generatePassword();
		
	}
	
	public Employee (RoleEnum role){
		this.role = role;
	}

	/**
	 * Method that informs the name of an employee.
	 * @return String with the name of the employee.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Method that changes the employee's name
	 * @param name: String
	 * @throws InvalidFieldEX
	 */
	public void setName(String name) throws InvalidFieldEX{
		if (name.trim().isEmpty()) {
			throw new InvalidFieldEX("Erro no cadastro de funcionario. Nome do funcionario nao pode ser vazio.");
		}

		this.name = name;
	}
	
	/**
	 * Method that informs the registration of an employee
	 * @return String with registration of the employee
	 */
	public String getRegistration() {
		return registration;
	}

	/**
	 * Method that changes the employee's registration
	 * @param registration: String
	 */
	public void setRegistration(String registration){
		this.registration = registration;
	}

	/**
	 * Method that informs the password of the employee.
	 * @return String with the employee's password.
	 */
	public String getPassword() {
		return password;
	}

	private void generatePassword() throws InvalidFieldEX{
		
		String pswrd = "";
		pswrd = pswrd +  this.getBirthDate().getYear();
		pswrd = pswrd + this.getRegistration().substring(0, 4);
		
		this.setPassword(pswrd);
		
	}
	
	/**
	 * Method that changes the employee's role
	 * @param role
	 */
	public void setRole(RoleEnum role) {
		this.role = role;
	}
	
	private void setPassword(String password) throws InvalidFieldEX {
		if(password == null){
			throw new InvalidFieldEX("Erro no cadastro de funcionario. Senha do funcionario nao pode ser null.");
		}
		if(password.trim().isEmpty()){
			throw new InvalidFieldEX("Erro no cadastro de funcionario. Nome do funcionario nao pode ser vazio.");
		}
		this.password = password;
	}
	
	/**
	 * Method that informs the birth date of the employee
	 * @return LocalDate with the employee's birth date.
	 */
	public LocalDate getBirthDate() {
		return birthDate;
	}
	
	/**
	 * Method that changes the employee's password.
	 * It receives the old password as a parameter, along with the new one.
	 * @param oldPsswrd: String with the old password
	 * @param newPsswrd: String with the new password
	 * @throws WrongPswrdEX
	 * @throws InvalidFieldEX
	 */
	public void changePassword(String oldPsswrd, String newPsswrd) throws WrongPswrdEX, InvalidFieldEX{
		if(!oldPsswrd.equals(this.getPassword())){
			throw new WrongPswrdEX("Erro ao atualizar funcionario. Senha invalida."); // alterar aqui depois
		}
		if(!newPsswrd.matches(SENHA_PATTERN_REGEX)){
			throw new InvalidFieldEX("Erro ao atualizar funcionario. A nova senha deve ter entre 8 - 12 caracteres alfanumericos.");
		}
		this.setPassword(newPsswrd);
	}
	
	/**
	 * Method that changes the employee's name
	 * @param name: String with the employee's name
	 * @throws InvalidFieldEX
	 */
	public void changeName(String name) throws InvalidFieldEX{
		if (name.trim().isEmpty()) {
			throw new InvalidFieldEX("Erro ao atualizar funcionario. Nome do funcionario nao pode ser vazio.");
		}
		if(name.matches(NAME_PATTERN_REGEX)){
			throw new InvalidFieldEX("Erro ao atualizar funcionario. Nome do funcionario nao pode ter mais que 16 caracteres.");
		}
		
		this.name = name;
	}
	
	/**
	 * Method that changes the employee's date
	 * @param date
	 * @throws InvalidFieldEX
	 */
	public void changeDate(String date) throws InvalidFieldEX{
		try{
			this.birthDate = getDate(date);
		}catch(InvalidFieldEX e){
			throw new InvalidFieldEX("Erro ao atualizar funcionario. Data invalida.");
		}
		
	}
	
	/**
	 * Method that changes the employee's birth date
	 * @param birthDate
	 * @throws InvalidFieldEX
	 */
	public void setBirthDate(LocalDate birthDate) throws InvalidFieldEX{

		this.birthDate = birthDate;
		
	}

	/**
	 * Method that informs the employee's role
	 * @return RoleEnum
	 */
	public RoleEnum getRole() {
		return role;
	}	
	
	private LocalDate getDate(String date) throws InvalidFieldEX{
		try{
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			LocalDate birth = LocalDate.parse(date, formatter);
			return birth;
		}catch(DateTimeParseException e){
			throw new InvalidFieldEX("Erro no cadastro de funcionario. Data invalida.");
		}

	}
	
	/**
	 * Abstract method that defines a registration generator.
	 * @param id: String
	 * @return String with the employee's ID
	 */
	public abstract String generateRegistration(String id);
	
}
